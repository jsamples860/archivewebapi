﻿CREATE PROCEDURE [dbo].[AddToAuditLog]
	@patientID nvarchar(128),
	@user nvarchar(128),
	@area nvarchar(50),
	@date DateTime,
	@log nvarchar(1024)

AS
	Insert into Audit (PatientID,[User],Area,[Date],Log) 
	values (@patientID,@user,@area,@date,@log)

