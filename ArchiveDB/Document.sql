﻿CREATE TABLE [dbo].[Document]
(
	[Id] BIGINT NOT NULL PRIMARY KEY IDENTITY,
	 [PatientID] UNIQUEIDENTIFIER NULL, 
    [Provider] NVARCHAR(128) NULL, 
    [Type] NVARCHAR(50) NULL,  
	  [Category] NVARCHAR(50) NULL,
    [Date] DATETIME NULL, 
	[Title] NVARCHAR(1024) NULL, 
    [Key] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [FK_Document_Patient] FOREIGN KEY (PatientID) REFERENCES Patient(Id)
)
