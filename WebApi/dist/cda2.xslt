<?xml version='1.0'?>

<!DOCTYPE xsl:stylesheet [
	<!ENTITY CDA-Stylesheet
	  '-//HL7//XSL HL7 V1.1 CDA Stylesheet: 2000-08-03//EN'>
	]>
<xsl:stylesheet version='1.0'
	xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:output method='html' indent='yes' version='4.01'
	doctype-public='-//W3C//DTD HTML 4.0 Transitional//EN'/>

<xsl:variable name='docType'
	select='/levelone/clinical_document_header/document_type_cd/@DN'/>
<xsl:variable name='orgName'
	select='/levelone/clinical_document_header/organization/@NM'/>
<xsl:variable name='titleOrgName'>
	<xsl:value-of select='$orgName'/>
</xsl:variable>
<xsl:variable name='titleDocType'>
	<xsl:value-of select='$docType'/>
</xsl:variable>

<xsl:template match='/levelone'>
	<html xmlns:iSvg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/1999/xhtml">
	<object id="AdobeSVG" classid="clsid:78156a80-c6a1-4bbf-8e6a-3cd390eeb4e2" style="display:none"></object>
	<xsl:text  disable-output-escaping="yes">&lt;?import namespace="iSvg" implementation="#AdobeSVG"?&gt;</xsl:text>
		<head>            
			<meta name='Generator' content='&CDA-Stylesheet;'/>
			<meta name='Copyright' content='Copyright 2000-2003 Greenway Medical Technologies, Inc. All Rights Reserved.'/>
			<script language="vbscript">
				Function document_onContextMenu()
					document_onContextMenu = false
					window.event.returnValue = false
					window.event.cancelBubble = true
				End Function
				
				document.body.style.display = "none"
			</script>
			<xsl:comment>
				do NOT edit this HTML directly, it was generated
				via an XSLT transformation from the original Level 1
				document.
			</xsl:comment>
			<style type="text/css">
				<xsl:comment > body
				{
					background-color: transparent;
					color: black;
					FONT-SIZE: 10pt;
					FONT-FAMILY: Tahoma;
				}
				<!-- div div
				{
					margin: 4px 0 4px 0.1in;
					padding: 0;
				}
				-->
				<!-- ul
				{
					margin: -4px 0 -8px 0;
					padding: 0;
				}
				-->
				div.paragraphHeader
				{
					margin: 0px 0 0px 10px;
					font-Size: 12px;
					font-Family: tahoma;
					padding-left: 0px;
				}

				div.paragraph
				{
					margin: 0px 0 5px 5px;
					font-Size: 12px;
					font-Family: tahoma;
					padding-left: 5px;
				}

				div.caption
				{
					font-weight: bold;
					text-decoration: underline;
					font-Size: 14px;
					font-Family: tahoma;
					letter-spacing: 2px;
					color: #003366;
				}

				span.caption
				{
					font-weight: bold;
					font-family: tahoma;
					font-size: 12px;
					color: #000000;
				}

				span.data
				{
					font-family: tahoma;
					font-size: 12px;
					color: #000000;
				}

				div.title
				{
					text-align: center;
					font-family: tahoma;
					font-size: 22px;
					color: #000033;
				}

				div.demographics
				{
					text-align: center;
					width: 100%;
				}

				div.demographicsslim
				{
					text-align: left;
					width: 100%;
					padding-left: 8px;
				}

				th.header
				{
					font-weight: bold;
					font-family: tahoma;
					font-size: 12px;
					color: #000000;
				}

				td.header
				{
					font-family: tahoma;
					font-size: 12px;
					color: #000000;
				}

				th
				{
					font-weight: bold;
					font-family: tahoma;
					font-size: 12px;
					color: #000000;
				}

				td
				{
					font-family: tahoma;
					font-size: 12px;
					color: #000000;
				}

				li.caption
				{
					font-weight: bold;
					font-family: tahoma;
					font-size: 12px;
					color: #000000;
				}

				li.subcaption
				{
					font-family: tahoma;
					font-size: 12px;
					color: #000000;
				}

				table.DataCell
				{
					text-align: left;
					padding-left: 0px;
					cursor: hand;
					font-size: 12px;
					font-family: tahoma;
					line-height: 20px;
				}

				td.subheadVitals
				{
					color: #707c88;
				}

				td.printclass
				{
					font-size: 8pt;
				}

				table
				{
					cell-padding: 0px;
				}

				A
				{
					COLOR: black;
					FONT-FAMILY: Tahoma;
					TEXT-DECORATION: none;
				}

					A:active
					{
						COLOR: black;
						FONT-FAMILY: Tahoma;
						TEXT-DECORATION: none;
					}

					A:link
					{
						COLOR: black;
						FONT-FAMILY: Tahoma;
						TEXT-DECORATION: none;
					}

					A:visited
					{
						COLOR: black;
						FONT-FAMILY: Tahoma;
						TEXT-DECORATION: none;
					}

				ADDRESS
				{
					COLOR: black;
					FONT-FAMILY: Tahoma;
					TEXT-DECORATION: none;
				}

				font.ABNORMALFONT
				{
					color: #b22222;
					font-style: italic;
				}

				.InputSpan
				{
					position: absolute;
					left: 0px;
					top: 0px;
					width: 698px;
					font-Size: 10pt;
					font-Family: tahoma;
					padding: 3px;
					z-Index: 100;
					color: black;
					background-color: transparent;
					border: 0;
					overflow-y: visible;
				}

				.InputSpanCustomNote
				{
					position: absolute;
					left: 0px;
					top: 0px;
					width: 698px;
					font-Size: 10pt;
					font-Family: tahoma;
					padding: 3px;
					z-Index: 100;
					color: black;
					background-color: transparent;
					border: 0;
					overflow-y: visible;
				}

				</xsl:comment >
			</style>
			
			<title>
				<xsl:if test='clinical_document_header/document_type_cd/@gmtid != "16"'>
					<xsl:text>[</xsl:text>
					<xsl:value-of select='$titleDocType'/><xsl:text>] [</xsl:text>
					<xsl:for-each select='/levelone/clinical_document_header/patient/person'>
						<xsl:call-template name='getName'/>
					</xsl:for-each>
					<xsl:text>] [</xsl:text>
					<xsl:value-of select='/levelone/clinical_document_header/patient/person/id/@EX'/>	
					<xsl:text>] </xsl:text>
				</xsl:if>
			
			</title>
		</head>
		<body  style="font-Family:tahoma;width:100%;">
			<div id='divBodyContainer' dir='ltr'>		
				<xsl:if test='clinical_document_header/document_type_cd/@gmtid = "16" or clinical_document_header/document_type_cd/@gmtid = "11"'>
				<br/>
				<br/>
				</xsl:if>
				
				<xsl:if test='clinical_document_header/document_type_cd/@gmtid = "11"'>
					<div class='title'>
                        <xsl:if test='/levelone/clinical_document_header/organization/@NM != ""'>
						    <xsl:value-of select='$titleOrgName'/>
                            <br />
                        </xsl:if>
                        <xsl:value-of select='$titleDocType'/>
					</div>
					<br/>
					<br/>
				</xsl:if>
				
				<xsl:if test='clinical_document_header/document_type_cd/@gmtid = "18"'>
					<xsl:apply-templates select='clinical_document_header' mode='slim'/>
				</xsl:if>
				
				<xsl:if test='clinical_document_header/document_type_cd/@gmtid != "18"  and clinical_document_header/document_type_cd/@gmtid != "16" and clinical_document_header/document_type_cd/@gmtid != "11"'>
					<div class='title'>
                        <xsl:if test='/levelone/clinical_document_header/organization/@NM != ""'>
						    <xsl:value-of select='$titleOrgName'/>
                            <br />
                        </xsl:if>
                        <xsl:value-of select='$titleDocType'/>
					</div>
					<br/>
					<xsl:apply-templates select='clinical_document_header' mode='normal'/>
					<br/>
				</xsl:if>
				
				<xsl:apply-templates select='body'/>
				
				<xsl:if test='clinical_document_header/document_type_cd/@gmtid != "16" and  clinical_document_header/document_type_cd/@gmtid != "11"'>
					<xsl:if test='clinical_document_header/document_type_cd/@gmtid != "18"' >
						<br/><br/><br/>
						<xsl:for-each select='/levelone/clinical_document_header/legal_authenticator/person'>
							
							<xsl:if test="position()=1">
								<xsl:call-template name='signature'/>
							</xsl:if>
							
							<xsl:if test="position()!=1">
								<xsl:call-template name='cosignature'/>
							</xsl:if>
							
						</xsl:for-each>
						<br/>
						<br/>
					</xsl:if>
					<xsl:if test='clinical_document_header/document_type_cd/@gmtid = "18"' >

						<xsl:for-each select='/levelone/clinical_document_header/legal_authenticator/person'>
							
							<xsl:if test="position()=1">
								<xsl:call-template name='signature'/>
							</xsl:if>
							
							<xsl:if test="position()!=1">
								<xsl:call-template name='cosignature'/>
							</xsl:if>
							
						</xsl:for-each>
						<br/>
					</xsl:if>
				</xsl:if>
			</div>
		</body>
	</html>
	<script type="text/javascript" src="/PrimePractice/common/Raphaeljs/json2.js"></script>
	<script type="text/javascript" src="/PrimePractice/common/Raphaeljs/raphael.js"></script>
	<script type="text/javascript" src="/PrimePractice/common/Raphaeljs/raphael.json.js"></script>
	<script type="text/javascript" language="javascript" >
		function ShiftCanvasForDrawingTool(sObjID) {
			//var svgdoc = document.all(sObjID).getSVGDocument();
			//var svgcanv = svgdoc.getElementById("canvas");

			//svgcanv.setAttribute("transform", "translate(-11, -41)");
		}


		function ShiftCanvasForMigratedDrawings(sObjID) {
			//var svgdoc = document.all(sObjID).getSVGDocument();
			//var svgcanv = svgdoc.getElementById("canvas");

			//svgcanv.setAttribute("transform", "translate(-240, -5)");
		}


		//Used to load up existing sketches(Path tags) onto the canvas
		function addSavedSketchToCanvas(oPath, sObjId, canvasHeight) {
		    var paper = new Raphael(document.getElementById(sObjId), 700, canvasHeight);
		    var oJSON = JSON.parse(oPath.getAttribute('data'));
		    var el;
		    for (var i in oJSON) {
		        el = oJSON[i];
		        if (el.type == 'path') {
		            paper.path(el.path).attr({
		                fill: el['fill'],
		                stroke: el['stroke'],
		                'stroke-opacity': el['stroke-opacity'],
		                'stroke-width': el['stroke-width'],
		                'stroke-linecap': el['stroke-linecap'],
		                'stroke-linejoin': el['stroke-linejoin'],
		                opacity: el['opacity'],
		                'fill-opacity': el['fill-opacity']
		            });
		        }
		    }
		}


		function SizeforCustomNote(sEmbedID, sHeight) {
			var svgdoc = document.all(sEmbedID).getSVGDocument();
			var svgcanv = svgdoc.getElementById("canvas");

			svgcanv.setAttribute("transform", "translate(-11, -41)");
		}


		function LoadWhiteBackground(sEmbedID, sCanvasHeight) {
			document.getElementById(sEmbedID).style.backgroundColor = 'white';
		}


		function AdjustTextAreaWithNoImageOrDrawing(sEmbedID, sTextAreaID) {
			document.all(sEmbedID).style.display = "none";
			document.all(sTextAreaID).style.position = "relative";
			document.all(sTextAreaID).style.height = "";
		}

		var sImageName;
		sImageName = "";
		function AddImageToCanvas(oImgNode, sEmbedID, sTextAreaID, sCanvasHeight) {
			var drawing = document.getElementById(sEmbedID);
			var drwImg;
			var image = document.createElement("img");

			if (sImageName != "") {
				ResizeCanvasImage();
			}

			if (sEmbedID.indexOf("customnote") != -1) {
				drwImg = document.getElementById(sEmbedID.replace("sketch", "image"));
				image.setAttribute("name", sEmbedID.replace("sketch", "image"));
				sImageName = sEmbedID.replace("sketch", "image");
			}
			if (sEmbedID.indexOf("drawing") != -1) {
				drwImg = document.getElementById(sEmbedID.replace("drawing", "drawingimage"));
				image.setAttribute("name", sEmbedID.replace("drawing", "drawingimage"));
				sImageName = sEmbedID.replace("drawing", "drawingimage");
			}
			image.src = oImgNode.getAttribute("image") + "?" + image.src.slice(5);
			image.style.position = "absolute";
			image.style.top = "1px";
			image.style.left = "1px";
			drwImg.style.display = "";
			drwImg.style.height = drawing.style.height;
			drwImg.style.width = drawing.style.width;
			drwImg.appendChild(image);

			drawing.style.backgroundColor = "white";
			drawing.style.height = sCanvasHeight;

			window.setTimeout("ResizeCanvasImage", 500);
		}

		function ResizeCanvasImage() {
			var img;
			var newHeight;
			img = document.getElementsByName(sImageName)[0];
			if (img.children[0].width > 695) {
				newHeight = img.children[0].height / img.children[0].width * 695;
				img.children[0].style.width = "695px";
				img.children[0].style.height = newHeight + "px";

				if (sImageName.indexOf("customnoteimage") > -1) {
					var obj = document.getElementById(sImageName.replace("customnoteimage", "customnotesketch"));
					var canvasHeight = obj.style.height;
					if (newHeight > canvasHeight.replace("px", "")) {
						obj.style.height = newHeight + "px";
					}
				}
			}
		}

		function AddCustomNoteImageToCanvas(oImgNode, sEmbedID, sTextAreaID, sCanvasHeight) {
			if (sImageName != "") {
				ResizeCanvasImage();
			}
			sImageName = sEmbedID.replace("sketch", "image");
			var drawing = document.getElementById(sEmbedID);
			var drwImg = document.getElementById(sEmbedID.replace("customnotesketch", "customnoteimage"));
			var image = document.createElement("img");
			image.src = oImgNode.getAttribute("image") + "?" + image.src.slice(5);
			image.style.position = "absolute";
			image.style.top = "1px";
			image.style.left = "1px";

			drwImg.style.display = "";
			drwImg.appendChild(image);

			drawing.style.backgroundColor = "white";

			window.setTimeout("ResizeCanvasImage", 250);
		}

		function addImageToSVG(sImage, sObjId) {
			//var target = document.all(sObjId).getOwnerDocument().getElementById('backgroundImage');  //SVGDoc
			var svgdoc = document.all(sObjId).getSVGDocument();
			var svgDSO = svgdoc.documentElement
			var target = svgdoc.getElementById("backgroundImage");

			var strHeight = "408"
			var strWidth = "400"
			var strData = sImage
			var oImg = target.getFirstChild()
			oImg.setAttribute("height", strHeight)
			oImg.setAttribute("width", strWidth)
			oImg.setAttribute("xlink:href", strData)
		}


		function loadPageBreaks() {
			var pageHeight = 910

			if (drawingsContainer) {

				//var oDIV = drawingsContainer.firstChild
				var oDIV = drawingsContainer.getElementsByTagName("DIV")(0)

				while (oDIV != null) {
					var strOffset = getpos(oDIV, 'top') //.offsetTop
					var pageNumber = parseInt(strOffset / pageHeight)
					var pageOffset = strOffset - (pageNumber * pageHeight)

					var oTextArea = oDIV.getElementsByTagName("TEXTAREA")(0);
					if (oTextArea != null) {
						var iTextHeight = parseInt(oTextArea.offsetHeight);

						//if(pageOffset > 615) //450
						if ((pageOffset + iTextHeight) > 910) {
							oDIV.style.pageBreakBefore = "always";
						}
					}
					oDIV = oDIV.nextSibling
				}
			}
		}


		function ValidateSVGDoc(sObjId) {
			//	        //var target = document.all(sObjId).getOwnerDocument().getElementById('backgroundImage');  //SVGDoc
			//	        var svgdoc = document.all(sObjId).getSVGDocument();
			//	        var svgDSO = svgdoc.documentElement;
			//	        var target = svgdoc.getElementById("backgroundImage");
			//	        //var objTest = target.isobject();
			//	        if (target == null) {
			//	            return false
			//	        }
			//	        else {
			//	            return true
			//	        }
		}


		function loadSketchPadPageBreaks() {
			var pageHeight = 910

			if (sketchPadsContainer) {
				var oDIV = sketchPadsContainer.firstChild

				while (oDIV != null) {
					var strOffset = getpos(oDIV, 'top') //.offsetTop
					var pageNumber = parseInt(strOffset / pageHeight)
					var pageOffset = strOffset - (pageNumber * pageHeight)

					//var iCanvasHeight = parseInt(oDIV.getElementsByTagName("EMBED")(0).getAttribute("height"));
					var iTextHeight = parseInt(oDIV.getElementsByTagName("TEXTAREA")(0).offsetHeight);
					//if(pageOffset > 615)
					if ((pageOffset + iTextHeight) > 910) {
						oDIV.style.pageBreakBefore = "always";
					}

					oDIV = oDIV.nextSibling;
				}
			}
		}

		function getIEVersion() {
			var userAgentString = navigator.userAgent;
			if (userAgentString.indexOf("MSIE 10.0") != -1 || userAgentString.indexOf("Trident/6.0") != -1) {
				return 10;
			} else if (userAgentString.indexOf("MSIE 9.0") != -1 || userAgentString.indexOf("Trident/5.0") != -1) {
				return 9;
			} else if (userAgentString.indexOf("MSIE 8.0") != -1 || userAgentString.indexOf("Trident/4.0") != -1) {
				return 8;
			} else if (userAgentString.indexOf("MSIE 7.0") != -1) {
				return 7;
			} else {
				return 6;
			}
		}

		var gsPath
		gsPath = "";
		function TransformPath(sPath, bShift) {
			gsPath = "";
			if (bShift) {
			    gsPath = Raphael.transformPath(sPath, 't-11,-45').toString();
			}
			else {
			    gsPath = Raphael.transformPath(sPath, 't0,0').toString();
			}
			gsPath = gsPath.replace("M", "M,");
			gsPath = gsPath.replace(/C/g, " ");
			gsPath = gsPath.replace(/L/g, " ");
			gsPath = gsPath.replace(/   /g, " ");
			gsPath = gsPath.replace(/  /g, " ");
			gsPath = gsPath.replace(/ /g, ",");
			gsPath = gsPath.replace(/,,/g, ",");
			var points = gsPath.split(",");
			gsPath = "";
			gsPath = points[0] + points[1];

			var i = 2;
			var done = false;
			do {
				if (i != points.length - 1) {
					gsPath = gsPath + "," + (points[i]) + "L" + (points[i + 1]);
				}
				else {
					gsPath = gsPath + "," + (points[i]);
					done = true;
				}
				i = i + 2
			}
			while (!done);
		}

		function WriteText(xmlSketchPads, sTextAreaID) {	       
			var oText, sText, oTextAreaObj;
			var xml = new ActiveXObject('Microsoft.XMLDom');
			xml.loadXML(xmlSketchPads.xml);
			oText = xml.selectSingleNode('//text');
			if (oText) {
				oTextAreaObj = document.getElementById(sTextAreaID);
				oTextAreaObj.value = oText.getAttribute('data');	            
			}
		}
	</script>

	
	<script language="vbscript">
		<![CDATA[		
		Function getPos(Byval el, Byval sProp)
			Dim iPos,iOffSet
			iPos = 0
			Do While not(el is nothing)
				Execute  "iOffSet = el.offset" & sProp
				iPos = iPos + iOffSet
				Set el = el.offsetParent
			Loop
			getPos = iPos
		End Function
	
		Function CheckforSVGViewer()
			on error resume next
			dim obj
			set obj = nothing
			set obj = createobject("Adobe.SVGCtl.3") 

			if err.number = 0 then
				CheckforSVGViewer = TRUE
			else
				CheckforSVGViewer = FALSE 
			end if
			'alert CheckforSVGViewer
			
			err.number = 0
			set obj = nothing
		end Function
		
		
		Function LoadSVGdata()
			dim oDrawings, oDrawing, oSketchPadsNode, oSketchPads, oSketchPad, oPaths, oPath
			dim sEmbedID, sTextAreaID, iTotalExpDist, iCanvasHeight
			dim bHasCanvas            
			if isobject(drawingxml) then
				'loadPageBreaks
				
				set oDrawings = drawingxml.documentelement.childNodes
	
				for each oDrawing in oDrawings
					set oSketchPads = oDrawing.selectNodes("sketchpads/sketchpad")                    
					if (oSketchPads.length > 0) then
						for each oSketchPad in oSketchPads
							sEmbedID = "drawing" & oDrawing.getattribute("refid") & "_" & oSketchPad.getAttribute("seq")
							document.all(sEmbedID).style.height = oSketchPad.getAttribute("canvasheight")
							sTextAreaID = "drawingtext" & oDrawing.getattribute("refid") & "_" & oSketchPad.getAttribute("seq")
							iTotalExpDist = oSketchPad.getAttribute("expandcount") * 40

							If Not isNull(oSketchPad.getAttribute("canvasheight")) Then
								iCanvasHeight = replace(oSketchPad.getAttribute("canvasheight"), "px", "") '+ iTotalExpDist
							Else
								iCanvasHeight = oSketchPad.parentNode.selectSingleNode("imglibgroup/imglibgroupitem[@seq='" & oSketchPad.getAttribute("seq") & "']").getAttribute("height")
							End If							
				
							set oCanvasNode = oSketchPad.selectSingleNode("canvas")

							dim oJSON, i
							set oJSON = oSketchPad.selectSingleNode("canvas/json")

							bHasCanvas = false
							
							if not oJSON is nothing then
								set oPath = oJSON
								if oPath.getAttribute("data") = "[]" then
									'do nothing
								else
									
									addSavedSketchToCanvas oPath, sEmbedID, iCanvasHeight
									bHasCanvas = true
								end if
							else
								if not oCanvasNode is nothing then
									set oPaths = oCanvasNode.childNodes
									dim sJSON
									i = 0
									sJSON = "["
									for each oPath in oPaths
										if oPath.nodeName = "path" then
											dim sPath, sStyle
											i = i + 1                                        
											TransformPath oPath.getAttribute("d"), true
											sPath = gsPath                                    
											sStyle = oPath.getAttribute("style")
											sStyle = ParseStyleToJSON(sStyle)
											sJSON = sJSON & "{" & sStyle & ", ""type"":""path"", ""path"":""" & sPath & """}"
											if not i = oPaths.length then
												sJSON = sJSON & ","
											end if 		
										end if		
                                        bHasCanvas = true					   
									next
									sJSON = sJSON & "]"

									dim oXML, oEl
									set oXML = createobject("msxml2.domdocument.6.0")
									set oEl = oXML.createElement("json")
									oEl.setAttribute "data", sJSON
									
									addSavedSketchToCanvas oEl, sEmbedID, iCanvasHeight									   									
								end if
							end if
							
							LoadBackground sEmbedID, sTextAreaID, oSketchPad.getAttribute("seq"), _
											oSketchPad.parentNode.selectSingleNode("imglibgroup"), _
											iCanvasHeight

							'make sure there is no imagelibgroup nor canvas...only text
							if oSketchPad.parentNode.selectSingleNode("imglibgroup") is nothing then
								if not bHasCanvas then
									AdjustTextAreaWithNoImageOrDrawing sEmbedID, sTextAreaID
								end if
							end if
		
							WriteText oSketchPad, sTextAreaID
						next
					else
						sEmbedID = "drawing" & oDrawing.getattribute("refid") & "_0"
						sTextAreaID = "drawingtext" & oDrawing.getattribute("refid") & "_0" 
						
						LoadBackground sEmbedID, sTextAreaID, 0, _
										oSketchPad.parentNode.selectSingleNode("imglibgroup"), _
										oSketchPad.getAttribute("canvasheight")

						AdjustTextAreaWithNoImageOrDrawing sEmbedID, sTextAreaID
						WriteText oSketchPad, sTextAreaID
					end if
				next
			end if
		End Function	

		function ParseStyleToJSON(sStyle)        
			dim styles, style, styleJSON, i
			styleJSON = ""
			styles = Split(sStyle, ";")
			
			for i = 0 to UBound(styles)
				style = styles(i)
				if style <> "" then
					if styleJSON <> "" then
						styleJSON = styleJSON & ","
					end if
					style = Split(style, ":")
					styleJSON = styleJSON & """" & style(0) & """:"
					if IsNumeric(style(1)) then
						styleJSON = styleJSON & style(1)
					else
						styleJSON = styleJSON & """" & style(1) & """"
					end if                    
				end if
			next
			ParseStyleToJSON = styleJSON
		End Function        
		
		sub LoadBackground(sEmbedID, sTextAreaID, sSeq, oImageGroupDom, sCanvasHeight)
			dim oImgNode
			
			if not oImageGroupDom is nothing then
				if oImageGroupDom.xml <> "" then
					set oImgNode = oImageGroupDom.selectSingleNode("imglibgroupitem[@seq='" & sSeq & "']")
				else
					set oImgNode = nothing
				end if
			else
				set oImgNode = nothing
			end if

			if not (oImgNode is nothing) then
				AddImageToCanvas oImgNode, sEmbedID, sTextAreaID, sCanvasHeight
			else
				LoadWhiteBackground sEmbedID, sCanvasHeight
			end if
		end sub


		Sub Window_OnLoad()
			document.body.style.display = "block"
'				'setTimeouts are ugly but they are needed to fix timing issues introduced by the new script engine in IE9
'				if getIEVersion() >= 9 then
'					setTimeout "LoadSVGdata", 500
'					
'					setTimeout "LoadSketchSVGCC", 125
'					setTimeout "LoadSketchSVGPSFH", 125
'					setTimeout "LoadSketchSVGROS", 125
'					setTimeout "LoadSketchSVGVitals", 125
'					setTimeout "LoadSketchSVGPE", 125
'					setTimeout "LoadSketchSVGIOP", 125
'					setTimeout "LoadSketchSVGAss", 125
'					setTimeout "LoadSketchSVGPlan", 125
'					setTimeout "LoadSketchSVGHPI", 125
'					setTimeout "FindSectionSVGTemplatedHPI", 125
'					setTimeout "LoadCustomNoteSVG", 125
'				else
					LoadSVGdata 
					
					LoadSketchSVGCC
					LoadSketchSVGPSFH
					LoadSketchSVGROS
					LoadSketchSVGVitals
					LoadSketchSVGPE
					LoadSketchSVGIOP
					LoadSketchSVGAss
					LoadSketchSVGPlan
					LoadSketchSVGHPI
					FindSectionSVGTemplatedHPI
					LoadCustomNoteSVG
'				end if
			window.settimeout "DefaultScroll",100  'no i could not call directly from here	
		End Sub
		
		
		sub LoadCustomNoteSVG
			dim oSketchPads, oSketchPad
			dim sSketchPadID
			dim bLoaded

			bLoaded = true
			
			if isobject(customnotesketchpadxml) = true then
				set oSketchPads = customnotesketchpadxml.documentElement.selectNodes("sketchpad")
						
				for each oSketchPad in oSketchPads
					sSketchPadID = "customnotesketch" & oSketchPad.getAttribute("seq")
'					if not ValidateSVGDoc(sSketchPadID) then
'						bLoaded = false
'					end if
				next
			end if

			if not bLoaded then
				window.setTimeout "LoadCustomNoteSVG", 250
				exit sub
			end if
			
			if isobject(customnotesketchpadxml) then
				LoadCustomNoteSectionSVGData
			end if
		end sub
		
		
		sub LoadCustomNoteSectionSVGData
			dim oSketchPads, oSketchPad, oPaths, oPath, oImgNode
			dim iTotalExpDist, iCanvasHeight
			dim sEmbedID, sTextAreaID
			dim oSketchPadDom
			dim bHasCanvas
			
			set oSketchPadDom = customnotesketchpadxml
			
			if isobject(oSketchPadDom) then
				set oSketchPads = oSketchPadDom.documentelement.selectNodes("sketchpad")

				'loadSketchPadPageBreaks
				for each oSketchPad in oSketchPads
					sEmbedID = "customnotesketch" & oSketchPad.getAttribute("seq")
					sTextAreaID = "customnotetext" & oSketchPad.getAttribute("seq")
					iTotalExpDist = oSketchPad.getAttribute("expandcount") * 40

					If Not isNull(oSketchPad.getAttribute("canvasheight")) Then
						iCanvasHeight = replace(oSketchPad.getAttribute("canvasheight"), "px", "") '+ iTotalExpDist
					Else
						iCanvasHeight = oSketchPad.parentNode.selectSingleNode("imglibgroup/imglibgroupitem[@seq='" & oSketchPad.getAttribute("seq") & "']").getAttribute("height")
					End If

					document.all(sEmbedID).style.height = oSketchPad.getAttribute("canvasheight")

					'add svg drawing to canvas
					set oCanvasNode = oSketchPad.selectSingleNode("canvas")
					
					bHasCanvas = false					

					dim oJSON
					set oJSON = oSketchPad.selectSingleNode("canvas/json")

					if not oJSON is nothing then
						set oPath = oJSON
						if oPath.getAttribute("data") = "[]" then
							'do nothing
						else
							addSavedSketchToCanvas oPath, sEmbedID, iCanvasHeight
							bHasCanvas = true
						end if
					else
						if not oCanvasNode is nothing then
							set oPaths = oCanvasNode.childNodes
							dim sJSON
							i = 0
							sJSON = "["
							for each oPath in oPaths
								if oPath.nodeName = "path" then
									dim sPath, sStyle
									i = i + 1                                        
									TransformPath oPath.getAttribute("d"), true
									sPath = gsPath                                    
									sStyle = oPath.getAttribute("style")
									sStyle = ParseStyleToJSON(sStyle)
									sJSON = sJSON & "{" & sStyle & ", ""type"":""path"", ""path"":""" & sPath & """}"
									if not i = oPaths.length then
										sJSON = sJSON & ","
									end if 		
								end if		
                                bHasCanvas = true					   
							next
							sJSON = sJSON & "]"

							dim oXML, oEl
							set oXML = createobject("msxml2.domdocument.6.0")
							set oEl = oXML.createElement("json")
							oEl.setAttribute "data", sJSON
									
							addSavedSketchToCanvas oEl, sEmbedID, iCanvasHeight														
						end if
					end if

					set oImgNode = oSketchPadDom.documentElement.selectSingleNode("imglibgroup/imglibgroupitem[@seq='" & oSketchPad.getAttribute("seq") & "']")

					if not (oImgNode is nothing) then
						LoadBackground sEmbedID, sTextAreaID, oSketchPad.getAttribute("seq"), _
								oSketchPadDom.documentElement.selectSingleNode("imglibgroup"), _
								iCanvasHeight
					else
						if not bHasCanvas then
							AdjustTextAreaWithNoImageOrDrawing sEmbedID, sTextAreaID
						end if
						LoadWhiteBackground sEmbedID, iCanvasHeight
						'SizeforCustomNote sEmbedID, "450"
					end if

					document.all(sTextAreaID).className = "InputSpanCustomNote"
					WriteText oSketchPad, sTextAreaID
				next
			end if
		end sub
		]]>


		sub FindSectionSVGTemplatedHPI
			dim oElement, oElements
			
			set oElements = document.getElementsByTagName("xml")
			
			for each oElement in oElements
				if left(oElement.getAttribute("id"), 8) = "parabody" then
					if not (LoadTemplatedHPISectionSVG (oElement)) then
						exit sub
					end if
				end if
			next
		end sub
		
		
		function LoadTemplatedHPISectionSVG(oXMLElement)
'			if not CheckSketchSVGDocLoaded(oXMLElement) then
'				window.setTimeout "FindSectionSVGTemplatedHPI", 250
'				LoadTemplatedHPISectionSVG = false
'				exit function
'			end if

			LoadSectionSVGdata(oXMLElement)
			LoadTemplatedHPISectionSVG = true
		end function


		sub LoadSketchSVGHPI
'			if not CheckSketchSVGDocLoaded(hpisketchpadxml) then
'				window.setTimeout "LoadSketchSVGHPI", 250
'				exit sub
'			end if
			LoadSectionSVGdata(hpisketchpadxml)
		end sub
		
		
		sub LoadSketchSVGPlan
'			if not CheckSketchSVGDocLoaded(plansketchpadxml) then
'				window.setTimeout "LoadSketchSVGPlan", 250
'				exit sub
'			end if
			LoadSectionSVGdata(plansketchpadxml)
		end sub
		
		
		sub LoadSketchSVGAss
'			if not CheckSketchSVGDocLoaded(asssketchpadxml) then
'				window.setTimeout "LoadSketchSVGAss", 250
'				exit sub
'			end if
			LoadSectionSVGdata(asssketchpadxml)
		end sub
		
		
		sub LoadSketchSVGIOP
'			if not CheckSketchSVGDocLoaded(iopsketchpadxml) then
'				window.setTimeout "LoadSketchSVGIOP", 250
'				exit sub
'			end if
			LoadSectionSVGdata(iopsketchpadxml)
		end sub
		
		
		sub LoadSketchSVGPE
'			if not CheckSketchSVGDocLoaded(pesketchpadxml) then
'				window.setTimeout "LoadSketchSVGPE", 250
'				exit sub
'			end if
			LoadSectionSVGdata(pesketchpadxml)
		end sub
		
		
		sub LoadSketchSVGVitals
'			if not CheckSketchSVGDocLoaded(vitalssketchpadxml) then
'				window.setTimeout "LoadSketchSVGVitals", 250
'				exit sub
'			end if
			LoadSectionSVGdata(vitalssketchpadxml)
		end sub
		
		
		sub LoadSketchSVGROS
'			if not CheckSketchSVGDocLoaded(rossketchpadxml) then
'				window.setTimeout "LoadSketchSVGROS", 250
'				exit sub
'			end if
			LoadSectionSVGdata(rossketchpadxml)
		end sub
		
		
		sub LoadSketchSVGPSFH
'			if not CheckSketchSVGDocLoaded(pfshsketchpadxml) then
'				window.setTimeout "LoadSketchSVGPSFH", 250
'				exit sub
'			end if
			LoadSectionSVGdata(pfshsketchpadxml)
		end sub
		
		
		sub LoadSketchSVGCC
'			if not CheckSketchSVGDocLoaded(ccsketchpadxml) then
'				window.setTimeout "LoadSketchSVGCC", 250
'				exit sub
'			end if
			LoadSectionSVGdata(ccsketchpadxml)
		end sub

        <![CDATA[
		Function LoadSectionSVGdata(oSketchPadDom)
			dim oSketchPads, oSketchPad, oPaths, oPath, oImgNode
			dim iTotalExpDist, iCanvasHeight
			dim sEmbedID
			dim bHasCanvas
			
			if isobject(oSketchPadDom) then
				set oSketchPads = oSketchPadDom.documentelement.childnodes
				
				for each oSketchPad in oSketchPads
					iTotalExpDist = oSketchPad.getAttribute("expandcount") * 40
					sEmbedID = oSketchPad.getAttribute("section") + "sketch" + oSketchPad.getAttribute("index")
					sTextAreaID = oSketchPad.getAttribute("section") + "text" + oSketchPad.getAttribute("index")                        

					if not isnull(oSketchPad.getAttribute("canvasheight")) then
						document.all(sEmbedID).style.height = oSketchPad.getAttribute("canvasheight")
						document.all(sTextAreaID).style.height = document.all(sEmbedID).style.height
					else
						document.all(sEmbedID).style.height = 280 '+ iTotalExpDist
						document.all(sTextAreaID).style.height = document.all(sEmbedID).style.height '+ iTotalExpDist
					end if

                    If Not isNull(oSketchPad.getAttribute("canvasheight")) Then
						iCanvasHeight = replace(oSketchPad.getAttribute("canvasheight"), "px", "") '+ iTotalExpDist
					Else
						iCanvasHeight = oSketchPad.parentNode.selectSingleNode("imglibgroup/imglibgroupitem[@seq='" & oSketchPad.getAttribute("seq") & "']").getAttribute("height")
					End If

                    bHasCanvas = false
					set oPaths = oSketchPad.selectSingleNode("canvas").childNodes
					
					dim oJSON
					set oJSON = oSketchPad.selectSingleNode("canvas/json")                    
					
					
					if not oJSON is nothing then
						set oPath = oJSON
						if oPath.getAttribute("data") = "[]" then
							'do nothing
						else
							addSavedSketchToCanvas oPath, sEmbedID, iCanvasHeight
							bHasCanvas = true
						end if
					else
                        dim sJSON
						i = 0
						sJSON = "["
						for each oPath in oPaths
                            if oPath.nodeName = "path" then
                                dim sPath, sStyle
								i = i + 1
                                TransformPath oPath.getAttribute("d"), false
                                sPath = gsPath
                                sStyle = ParseStyleToJSON(oPath.getAttribute("style"))
                                sJSON = sJSON & "{" & sStyle & ", ""type"":""path"", ""path"":""" & sPath & """}"
                                if not i = oPaths.length then
									sJSON = sJSON & ","
								end if 	
                            end if
                            bHasCanvas = true
						next
                        sJSON = sJSON & "]"

						dim oXML, oEl
						set oXML = createobject("msxml2.domdocument.6.0")
						set oEl = oXML.createElement("json")
						oEl.setAttribute "data", sJSON
									
						addSavedSketchToCanvas oEl, sEmbedID, iCanvasHeight						
					end if

					set oImgNode = oSketchPad.selectSingleNode("image")
					if not (oImgNode is nothing) then
						AddImageToCanvas oImgNode, sEmbedID, sTextAreaID, iCanvasHeight
					else
						if not bHasCanvas then
							AdjustTextAreaWithNoImageOrDrawing sEmbedID, sTextAreaID
						else
							LoadWhiteBackground sEmbedID, cstr(document.all(sEmbedID).style.height)
						end if
					end if
					'document.all(sTextAreaID).value = document.all(sTextAreaID).getAttribute("data")
					WriteText oSketchPad, sTextAreaID
				next
				'loadSketchPadPageBreaks
			end if
		End Function	
		]]>
		
		function CheckSketchSVGDocLoaded(oSketchPadDom)
			dim oSketchPads, oSketchPad
			dim sSketchPadID
			dim bLoaded
			
			if isobject(oSketchPadDom) then
				set oSketchPads = oSketchPadDom.documentElement.childNodes
						
				for each oSketchPad in oSketchPads
					sSketchPadID = oSketchPad.getAttribute("section") + "sketch" + oSketchPad.getAttribute("index")
					if not ValidateSVGDoc(sSketchPadID) then
						CheckSketchSVGDocLoaded = false
						exit function
					end if
				next
			end if
			
			CheckSketchSVGDocLoaded = true
		end function


		Sub DefaultScroll()
			document.body.scrollleft = -0
		End Sub


		sub CancelBubble()
			window.event.cancelBubble = true
			window.event.returnValue = false
		end sub

		Function NullHandler(value)
			if isnull(value) then
				NullHandler = ""
			else
				NullHandler = value
			end if
		End Function
	</script>


</xsl:template>

<!--
	 generate a table at the top of the document containing
	 encounter and patient information.  Encounter info is
	 rendered in the left column(s) and patient info is
	 rendered in the right column(s).
	 
	 This assumes several things about the source document which
	 won't be true in the general case:
	 
		1. there is only 1 of everything (i.e., physcian, patient, etc.)
		2. I haven't bothered to map all HL7 table values
		   (e.g., actor.type_cd and administrative_gender_cd)
		   and have only those that are used in the sample document
		
		I tried to do the table formting with CSS2 rules, but Netscape
		doesn't seem to handle the table rules well (or at all:-( so I
		just gave up
  -->
<xsl:template match='clinical_document_header' mode='normal'>
	<div class='demographics' >
		<table border='0' align='left'>
			<tr>
				<td align='left' vAlign='top'>
					<table border='0' width='330px'>
						<tbody>
							<xsl:call-template name='patient'/>
						</tbody>
					</table>
				</td>
				<td align='left' vAlign='top'>
					<table border='0' width='380px' >
						<tbody>
							<xsl:call-template name='encounter'/>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
	</div>
</xsl:template>

<xsl:template match='clinical_document_header' mode='slim'>
	<div class='demographicsslim' >
		<table border='0' width='700' style='table-layout:fixed;'>
			<tbody>
				<tr>
					<xsl:call-template name='patient_slim'/>
				</tr>
				<tr>
					<xsl:call-template name='encounter_slim'/>
				</tr>
			</tbody>
		</table>			
	</div>
</xsl:template>
	
<xsl:template name='encounter_slim'>
		<xsl:apply-templates select='patient_encounter/encounter_tmr'/>
	
		<xsl:apply-templates select='provider'/>
	
		<xsl:apply-templates select='patient_encounter/service_location'/>
		
		<xsl:apply-templates select='patient_encounter/location_address'/>
		
		<xsl:apply-templates select='patient_encounter/phone_number'/>
</xsl:template>

	
<xsl:template name='encounter'>
	<tr>
		<xsl:apply-templates select='patient_encounter/encounter_tmr'/>
	</tr>
	<tr>
		<xsl:apply-templates select='provider'/>
	</tr>
	<tr>
		<xsl:apply-templates select='patient_encounter/service_location'/>
	</tr>
	<tr>
		<xsl:apply-templates select='patient_encounter/location_address'/>
	</tr>
	<tr>
		<xsl:apply-templates select='patient_encounter/phone_number'/>
	</tr>
</xsl:template>

<xsl:template match='encounter_tmr'>
<xsl:if test='@visit = "true"'>		
	<th align='left' width='140px'  class='header'>Visit Date:</th>
</xsl:if>
<xsl:if test='@visit = "false"'>		
	<th align='left' width='140px'  class='header'>Create Date:</th>
</xsl:if>

	<td align='left' width='250px'  class='header'>
		<xsl:call-template name='date'>
			<xsl:with-param name='date' select='@V'/>
		</xsl:call-template>
	</td>
</xsl:template>

<xsl:template match='service_location'>
	<th align='left' width='140px'  class='header'>Location:</th>
	<td align='left' width='250px'  class='header'>
		<xsl:value-of select='@V'/>
	</td>
</xsl:template>

<xsl:template match='location_address'>
	<th align='left' width='140px'  class='header' style="vertical-align: top;">Location Address:</th>
	<td align='left' width='250px'  class='header'>
		<xsl:value-of select='address_line_1/@V'/><br />
		<xsl:if test='address_line_2/@V != ""'>
			<xsl:value-of select='address_line_2/@V'/><br />
		</xsl:if>
		<xsl:value-of select='city/@V'/>
		<xsl:text disable-output-escaping="yes"><![CDATA[,&nbsp;]]></xsl:text>
		<xsl:value-of select='state/@V'/>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;]]></xsl:text>
		<xsl:value-of select='postal_code/@V'/>
	</td>
</xsl:template>

<xsl:template match='phone_number'>
	<xsl:if test='@V != ""'>
		<th align='left' width='140px'  class='header'>Location Phone:</th>
		<td align='left' width='250px'  class='header'>
			<xsl:value-of select='@V'/>
		</td>
	</xsl:if>
</xsl:template>

<xsl:template match='provider'>
	<th align='left' width='140px'>
		<xsl:call-template name='provider_type_cd'>
			<xsl:with-param name='type_cd' select='provider.type_cd/@V'/>
		</xsl:call-template>
		<xsl:text>Provider:</xsl:text>
	</th>
	<td align='left' width='250px'>
		<xsl:variable name='ptr' select='person/id/@EX'/>

		<xsl:for-each select='/levelone/clinical_document_header/provider/person[id/@EX=$ptr]'>
			<xsl:call-template name='getName'/>
		</xsl:for-each>
	</td>
</xsl:template>

<xsl:template name='patient' >
	<xsl:apply-templates select='patient' mode='normal'/>
	<tr>
		<xsl:apply-templates select='patient/birth_dttm'/>
	</tr>
    <tr>
        <xsl:apply-templates select='patient/pcp_name'/>
    </tr>
    <tr>
        <xsl:apply-templates select='patient/referring_name'/>
    </tr>
</xsl:template>

<xsl:template name='patient_slim'>
	<xsl:apply-templates select='patient' mode='slim'/>
</xsl:template>


<xsl:template match='birth_dttm'>
	<th align='left' width='185px' class='header'>Birthdate:</th>
	<td align='left' width='260px' class='header'>
		<xsl:call-template name='date'>
			<xsl:with-param name='date' select='@V'/>
		</xsl:call-template>
	</td>
</xsl:template>

<xsl:template match='pcp_name'>
    <xsl:if test='@V != ""'>
        <th align='left' width='185px' class='header'>Primary Care Provider:</th>
		<td align='left' width='260px'  class='header'>
			<xsl:value-of select='@V'/>
		</td>
    </xsl:if>
</xsl:template>

<xsl:template match='referring_name'>
    <xsl:if test='@V != ""'>
        <th align='left' width='185px' class='header'>Referring Provider:</th>
		<td align='left' width='260px'  class='header'>
			<xsl:value-of select='@V'/>
		</td>
    </xsl:if>
</xsl:template>

<xsl:template match='patient' mode='normal'>
	<tr>
		<th align='left' width='185px' class='header'>Patient Name:</th>
		<td align='left' width='260px' class='header'>
			<xsl:for-each select='person'>
				<xsl:call-template name='getName'/>
			</xsl:for-each>
		</td>
	</tr>
	<tr>
		<th align='left' width='185px' class='header'>
			<xsl:text>Patient ID:</xsl:text>
		</th>
		<td align='left' width='260px' class='header'>
			<xsl:value-of select='person/id/@EX'/>				
		</td>
	</tr>
	<tr>
		<th align='left' width='185px' class='header'>Sex:</th>
		<td align='left' width='260px' class='header'>
			<xsl:call-template name='administrative_gender_cd'>
				<xsl:with-param name='gender_cd' select='administrative_gender_cd/@V'/>
			</xsl:call-template>
		</td>
	</tr>
</xsl:template>


<xsl:template match='patient' mode='slim'>
	
		<th align='left' width='96px' class='header'>Patient Name:</th>
		<td align='left' width='153px' class='header'>
			<xsl:for-each select='person'>
				<xsl:call-template name='getName'/>
			</xsl:for-each>
		</td>
	
		<th align='left' width='80px' class='header'>
			<xsl:text>Patient ID:</xsl:text>
		</th>
		<td align='left' width='153px' class='header'>
			<xsl:value-of select='person/id/@EX'/>				
		</td>
	
		<th align='left' width='60px' class='header'>Sex:</th>
		<td align='left' width='155px' class='header'>
			<xsl:call-template name='administrative_gender_cd'>
				<xsl:with-param name='gender_cd' select='administrative_gender_cd/@V'/>
			</xsl:call-template>
		</td>
</xsl:template>


<!--
	just apply the default template for these
  -->
<xsl:template match='body|caption|content|notes'>
	<xsl:apply-templates/>
</xsl:template>

<!--
	spit out the caption (in the 'caption' style)
	followed by applying whatever templates we
	have for the applicable children
  -->
<xsl:template match='section'>
	<div>
		<xsl:if test='caption != "Custom Note"'>
			<div class='caption'>
				<xsl:apply-templates select='caption'/>
			</div>
		</xsl:if>
		
		
		<xsl:apply-templates select='paragraph|list|table|section|local_markup'/>
	</div><br/>
</xsl:template>

<xsl:template match='section/section'>
	<ul style='margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;'>
		<li style='list-style-type:none;' >
			<span class='caption' style='display:inline-block;width:250px;'>
				<xsl:apply-templates select='caption'/>
				<xsl:if test="@caption!=''">:</xsl:if>
			</span>
			<xsl:apply-templates select='paragraph|list|table|section'/>
		</li>
	</ul>
</xsl:template>

<xsl:template match='focused'>
	<b>
		<i>
			<xsl:apply-templates/>
		</i>
	</b>
</xsl:template>


<!--
	currently ignores paragraph captions...
	
	I need samples of the use description and render
	to know what really should be done with them
  -->
<xsl:template match='paragraph'>
	<xsl:if test="@title != ''">
			<div  class='paragraphHeader'  style='font-weight:bold;width:85%'>
			<xsl:value-of select='@title'/>
			</div>
	</xsl:if>
	<xsl:if test="@title = ''">
			<div  class='paragraphHeader'  style='font-weight:bold;width:85%'>
			</div>
	</xsl:if>
	<div class='paragraph' style='width:85%'>
		<xsl:apply-templates select='content'/>
        <br />
        <xsl:apply-templates select='notes' />
	</div>
	<xsl:apply-templates select='local_markup'/>
</xsl:template>

<xsl:template match='bolditalic'>
	<b><i>
		<xsl:apply-templates select='content'/>
	</i></b>
</xsl:template>

<xsl:template match='font'>
	<font class='ABNORMALFONT'>
		<xsl:apply-templates/>
	</font>
</xsl:template>

<!--
	currently ignore caption's on the list itself,
	but handles them on the list items
  -->
<!--<xsl:template match='list'>
	<ul>
		<xsl:for-each select='item'>
			<li >
				<xsl:if test='caption'>
					<span class='caption'>
						<xsl:apply-templates select='caption'/>
                        <xsl:apply-templates select='notes'/>
					</span>
					<xsl:text> : </xsl:text>
				</xsl:if>
				<xsl:apply-templates select='content'/>
                <br /><span id="indent" style="width:7px;" />
                <xsl:apply-templates select='notes'>
                </xsl:apply-templates>
			</li>
		</xsl:for-each>
	</ul>
</xsl:template>-->

<xsl:template match="list">
    <ul>
        <xsl:for-each select="item">
            <li>
                <xsl:if test="caption">
				    <span class="caption">
					    <xsl:apply-templates select="caption"/>
                        <xsl:if test="/notes">
                            <xsl:apply-templates select="notes"/>
                        </xsl:if>
					</span>
					<xsl:text> : </xsl:text>
				</xsl:if>
            
                <xsl:apply-templates select="content" />
            
                <xsl:if test="notes">
                    <xsl:apply-templates select="notes/note" />
                </xsl:if>
			</li>
		</xsl:for-each>
	</ul>
</xsl:template>

<!-- notes sublist -->
<xsl:template match="notes/note">
    <ul style="list-style-type:none;">
        <li>
            <xsl:value-of select="."></xsl:value-of>
        </li>
    </ul>
</xsl:template>




<xsl:template match='section/section/paragraph'>
	<span class='data' >
		<xsl:apply-templates/>
	</span>
</xsl:template>

<!-- 
	 Tables
	 
	 just copy over the entire subtree, as is
	 except that the children of CAPTION are possibly handled by
	 other templates in this stylesheet
  -->
  
 <xsl:template match='table'>
	<xsl:copy>
		<xsl:apply-templates select='*|@*|text()'/>
	</xsl:copy>
	<BR/>
</xsl:template> 

<xsl:template match='th'>
	<xsl:copy>
		
		<xsl:attribute name="class">RowHeader</xsl:attribute>
		<xsl:apply-templates select='*|@*|text()'/>
	</xsl:copy>
</xsl:template>



<xsl:template match='caption|thead|tfoot|tbody|colgroup|col|tr|td|br|notes'>
	<xsl:copy>
		<xsl:apply-templates select='*|@*|text()'/>
	</xsl:copy>
	
</xsl:template>

<xsl:template match='table/@*|thead/@*|tfoot/@*|tbody/@*|colgroup/@*|col/@*|tr/@*|th/@*|td/@*'>
	<xsl:copy>
		<xsl:apply-templates/>
	</xsl:copy>
</xsl:template>

<!-- This is to preserve style attributes on <div>s in <td>s, rather than having them wiped by other templates. This allows for "variable width" columns -->
<xsl:template match="div[@class='subCell']">
    <div>
        <xsl:attribute name="style">
            <xsl:value-of select="@style" />
        </xsl:attribute>
	    <xsl:apply-templates />
    </div>
</xsl:template>

<!--
	 this currently only handles GIF's and JPEG's.  It could, however,
	 be extended by including other image MIME types in the predicate
	 and/or by generating <object> or <applet> tag with the correct
	 params depending on the media type
  -->
<xsl:template match='observation_media'>
	<xsl:if test='observation_media.value[
			@MT="image/gif" or @MT="image/jpeg"
			]'>
		<br clear='all'/>
		<xsl:element name='img'>
			<xsl:attribute name='src'>
				<xsl:value-of select='observation_media.value/REF/@V'/>
			</xsl:attribute>
		</xsl:element>
	</xsl:if>
</xsl:template>

<!--
	turn the link_html subelement into an HTML a element,
	complete with any attributes and content it may have,
	while stripping off any CDA specific attributes
  -->
<xsl:template match='link'>
	<xsl:element name='a'>
		<xsl:for-each select='link_html/@*'>
			<xsl:if test='not(name()="originator" or name()="confidentiality")'>
				<xsl:attribute name='{name()}'>
					<xsl:value-of select='.'/>
				</xsl:attribute>
			</xsl:if>
		</xsl:for-each>
		<xsl:value-of select='link_html'/>
	</xsl:element>
</xsl:template>

<!--
	this doesn't do anything with the description
	or render attributes...it simply decides whether
	to remove the entire subtree or just pass the
	content thru
	
	I need samples of the use description and render
	to know what really should be done with them
  -->

<xsl:template match='local_markup'>
	
	<xsl:apply-templates select='datahtml/drawings' />
	
	<span language='vbscript' onclick='window.event.returnvalue=false'>
		<xsl:value-of select='datahtml' disable-output-escaping="yes"/>
	</span>
	
	<xsl:apply-templates select='datahtml/sketchpads' />
</xsl:template>

<xsl:template match="datahtml/drawings">
	<div id="drawingsContainer">
		<xsl:for-each select='drawing'>
			<xsl:for-each select='sketchpads/sketchpad'>            
				<span style='font-weight:bold;'>
					Figure <xsl:for-each select="../..">
							  <xsl:number/>
							</xsl:for-each>
					<xsl:choose>
						<xsl:when test='./@seq != ""'><xsl:text>.</xsl:text><xsl:value-of select="./@seq"/></xsl:when>
						<xsl:otherwise></xsl:otherwise>
					</xsl:choose>:
				</span>
				<span style='padding-left:5px;'>
					<xsl:value-of select="../@title"/>
				</span>
				<div>
					<table>
					<tr>
						<td style="position:relative; top:0px; left:0px;">                     
							<!--<EMBED style="border:1px solid #000000" wmode='transparent' width="700" height="400" src="/PrimePractice/Clinical/SketchPad/SketchPadPreview.svg">-->                            
							<div style='width:700px; height:400px; position:absolute; display:none'>
								<xsl:attribute name="id">
									<xsl:text>drawingimage</xsl:text>
									<xsl:value-of select="../../@refid"/>_<xsl:choose>
																			<xsl:when test='./@seq != ""'><xsl:value-of select="./@seq"/></xsl:when>
																				<xsl:otherwise>0</xsl:otherwise>
																			</xsl:choose>
								</xsl:attribute>
							</div>
							<div style='border:solid #003366 1px; background-color:transparent; width:700px; height:400px'>
								<xsl:attribute name="id">
									<xsl:text>drawing</xsl:text>
									<xsl:value-of select="../../@refid"/>_<xsl:choose>
																			<xsl:when test='./@seq != ""'><xsl:value-of select="./@seq"/></xsl:when>
																				<xsl:otherwise>0</xsl:otherwise>
																			</xsl:choose>
								</xsl:attribute>
								<xsl:attribute name="name">
									<xsl:text>drawing</xsl:text>
									<xsl:value-of select="../../@refid"/>_<xsl:choose>
																		<xsl:when test='./@seq != ""'><xsl:value-of select="./@seq"/></xsl:when>
																			<xsl:otherwise>0</xsl:otherwise>
																		</xsl:choose>
								</xsl:attribute>
							</div>
							<!--</EMBED>-->
							<textarea class="InputSpan" onselectstart="cancelbubble" onbeforeactivate="cancelbubble" onbeforeeditfocus="cancelbubble">
								<xsl:attribute name="id">
									<xsl:text>drawingtext</xsl:text>
									<xsl:value-of select="../../@refid"/>_<xsl:choose>
																		<xsl:when test='./@seq != ""'><xsl:value-of select="./@seq"/></xsl:when>
																			<xsl:otherwise>0</xsl:otherwise>
																		</xsl:choose>
								</xsl:attribute>
								<xsl:attribute name="data">
									<xsl:value-of select="text/@data"/>
								</xsl:attribute>
								<xsl:value-of select="./text/@data"/>
							</textarea>	                                                       						
						</td>
					</tr>
					</table>
				</div>            
			</xsl:for-each>
		</xsl:for-each>
	</div>
</xsl:template>


<xsl:template match="datahtml/sketchpads">
	<div id="sketchPadsContainer" style="padding-left:5px;">
		<xsl:for-each select='sketchpad'>
			
				<table>
				<tr>
					<td>
						<xsl:if test='../../../../caption = "Custom Note"'>
							<table>
							<tr>
								<td><span style="font-weight:bold; padding-right:10px;">Title:</span></td>
								<td><xsl:value-of select="../@title"/></td>
								<td width="100"></td>

								<td><span style="font-weight:bold; padding-right:10px;">DocType:</span></td>
								<td><xsl:value-of select="../@doctypename"/></td>
							</tr>
							</table>
						</xsl:if>
					</td>
				</tr>
				<tr>
					<td style="position:relative; top:0px; left:0px;">
						<!--<EMBED style="border:solid #003366 1px;" wmode='transparent' width="700" height="400" src="/PrimePractice/Clinical/SketchPad/SketchPadPreview.svg">-->
						<div style='width:700px; height:400px; position:absolute; display:none'>
							<xsl:attribute name="id">
								<xsl:choose>
									<xsl:when test='../../../../caption = "Custom Note"'>customnote</xsl:when>
									<xsl:otherwise><xsl:value-of select="@section"/></xsl:otherwise>
								</xsl:choose>
							
								<xsl:text>image</xsl:text>
								<xsl:if test='@seq != ""'><xsl:value-of select="@seq"/></xsl:if>
								<xsl:if test='@index != ""'><xsl:value-of select="@index"/></xsl:if>
							</xsl:attribute>
						</div>
						<div style='border:solid #003366 1px; background-color:transparent; width:700px; height:400px'>
							<xsl:attribute name="id">
								<xsl:choose>
									<xsl:when test='../../../../caption = "Custom Note"'>customnote</xsl:when>
									<xsl:otherwise><xsl:value-of select="@section"/></xsl:otherwise>
								</xsl:choose>
							
								<xsl:text>sketch</xsl:text>
								<xsl:if test='@seq != ""'><xsl:value-of select="@seq"/></xsl:if>
								<xsl:if test='@index != ""'><xsl:value-of select="@index"/></xsl:if>
							</xsl:attribute>

							<xsl:attribute name="name">
								<xsl:choose>
									<xsl:when test='../../../../caption = "Custom Note"'>customnote</xsl:when>
									<xsl:otherwise><xsl:value-of select="@section"/></xsl:otherwise>
								</xsl:choose>
								
								<xsl:text>sketch</xsl:text>
								<xsl:if test='@seq != ""'><xsl:value-of select="@seq"/></xsl:if>
								<xsl:if test='@index != ""'><xsl:value-of select="@index"/></xsl:if>
							</xsl:attribute>
						</div>
						<!--</EMBED>-->
						<div onselectstart="cancelbubble" onbeforeactivate="cancelbubble" onbeforeeditfocus="cancelbubble" style="width:698px;word-wrap:break-word;position:absolute;top:0px;left:0px;z-index:100;padding:3px;">
							<xsl:attribute name="id">
								<xsl:choose>
									<xsl:when test='../../../../caption = "Custom Note"'>customnote</xsl:when>
									<xsl:otherwise><xsl:value-of select="@section"/></xsl:otherwise>
								</xsl:choose>

								<xsl:text>text</xsl:text>
								<xsl:if test='@seq != ""'><xsl:value-of select="@seq"/></xsl:if>
								<xsl:if test='@index != ""'><xsl:value-of select="@index"/></xsl:if>
							</xsl:attribute>							
							<pre style="white-space:pre-wrap;font-size:10pt;font-family:tahoma;"><xsl:value-of select="text/@data"/></pre>									
						</div>
					</td>
				</tr>
				</table>

		</xsl:for-each>
	</div>
</xsl:template>


<!--
<xsl:template match='local_markup'>
	<xsl:value-of select='datahtml' disable-output-escaping="yes"/>
	
</xsl:template>
-->

<!--
	 elements to ignore
  -->
<xsl:template match='coded_entry'>
</xsl:template>

<xsl:template match='caption_cd'>
</xsl:template>


<!--
	 template(s) to output signature block
  -->
<xsl:template name='signature'>
	<xsl:variable name='signers' select='.'/>
	<xsl:if test='$signers'>
		<div>
			<span class='caption'>Electronically Signed by: </span>
			<xsl:for-each select='$signers'>
				<xsl:call-template name='getName'>
					<xsl:with-param name='person' select='.'/>
				</xsl:call-template>
				<xsl:text> -Author on </xsl:text>	
				
				<xsl:call-template name='date'>
					<xsl:with-param name='date' select='../participation_tmr/@V'/>
				</xsl:call-template>

			  
			</xsl:for-each>
		</div>
	</xsl:if>
</xsl:template>


<xsl:template name='cosignature'>
	<xsl:variable name='signers' select='.'/>
	<xsl:if test='$signers'>
		<div>
			<span class='caption'>Electronically Co-signed by: </span>
			<xsl:for-each select='$signers'>
				<xsl:call-template name='getName'>
					<xsl:with-param name='person' select='.'/>
				</xsl:call-template>
				<xsl:text> -Reviewer on </xsl:text>	
				
				<xsl:call-template name='date'>
					<xsl:with-param name='date' select='../participation_tmr/@V'/>
				</xsl:call-template>
			  
			</xsl:for-each>
		</div>
	</xsl:if>
</xsl:template>

<!--
	 general purpose (named) templates used in multiple places
  -->
<!--
	 assumes current node is a <person_name> node

	 Does not handle nearly all of the complexity of the person datatype,
	 but is illustritive of what would be required to do so in the future
  -->
<xsl:template name='getName'>
	<xsl:apply-templates select='person_name[person_name.type_cd/@V="L"]'/>
</xsl:template>
<xsl:template match='person_name'>
	<xsl:choose>
		<xsl:when test='nm/GIV[@QUAL="RE"]/@V'>
			<xsl:value-of select='nm/GIV[@QUAL="RE"]/@V'/>
		</xsl:when>
		<xsl:when test='nm/GIV[@CLAS="N"]/@V'>
			<xsl:value-of select='nm/GIV[@CLAS="N"]/@V'/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select='nm/GIV/@V'/>
		</xsl:otherwise>
	</xsl:choose>

	<xsl:choose>
		<xsl:when test='nm/MID[@QUAL="RE"]/@V'>
			<xsl:text> </xsl:text>
			<xsl:value-of select='substring (nm/MID[@QUAL="RE"]/@V, 1, 1)'/>
			<xsl:text>.</xsl:text>
		</xsl:when>
		<xsl:when test='nm/MID/@V != ""'>
			<xsl:text> </xsl:text>
			<xsl:value-of select='substring (nm/MID/@V, 1, 1)'/>
			<xsl:text>.</xsl:text>
		</xsl:when>
	</xsl:choose>

	<xsl:choose>
		<xsl:when test='nm/FAM[@QUAL="RE"]/@V'>
			<xsl:text> </xsl:text>
			<xsl:value-of select='nm/FAM[@QUAL="RE"]/@V'/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text> </xsl:text>
			<xsl:value-of select='nm/FAM/@V'/>
		</xsl:otherwise>
	</xsl:choose>

	<xsl:choose>
		<xsl:when test='nm/SFX[@QUAL="RE"]/@V'>
			<xsl:text>, </xsl:text>
			<xsl:value-of select='nm/SFX[@QUAL="RE"]/@V'/>
		</xsl:when>
		<xsl:when test='nm/SFX/@V != ""'>
			<xsl:text>, </xsl:text>
			<xsl:value-of select='nm/SFX/@V'/>
		</xsl:when>
	</xsl:choose>
	
</xsl:template>

<!--
	 outputs a date in Month Day, Year form
	 
	 e.g., 19991207  ==> December 07, 1999
  -->
<xsl:template name='date'>
	<xsl:param name='date'/>
	<xsl:variable name='month' select='substring ($date, 6, 2)'/>
	<xsl:choose>
		<xsl:when test='$month=01'>
			<xsl:text>January </xsl:text>
		</xsl:when>
		<xsl:when test='$month=02'>
			<xsl:text>February </xsl:text>
		</xsl:when>
		<xsl:when test='$month=03'>
			<xsl:text>March </xsl:text>
		</xsl:when>
		<xsl:when test='$month=04'>
			<xsl:text>April </xsl:text>
		</xsl:when>
		<xsl:when test='$month=05'>
			<xsl:text>May </xsl:text>
		</xsl:when>
		<xsl:when test='$month=06'>
			<xsl:text>June </xsl:text>
		</xsl:when>
		<xsl:when test='$month=07'>
			<xsl:text>July </xsl:text>
		</xsl:when>
		<xsl:when test='$month=08'>
			<xsl:text>August </xsl:text>
		</xsl:when>
		<xsl:when test='$month=09'>
			<xsl:text>September </xsl:text>
		</xsl:when>
		<xsl:when test='$month=10'>
			<xsl:text>October </xsl:text>
		</xsl:when>
		<xsl:when test='$month=11'>
			<xsl:text>November </xsl:text>
		</xsl:when>
		<xsl:when test='$month=12'>
			<xsl:text>December </xsl:text>
		</xsl:when>
	</xsl:choose>
	<xsl:choose>
		<xsl:when test='substring ($date, 9, 1)="0"'>
			<xsl:value-of select='substring ($date, 10, 1)'/><xsl:text>, </xsl:text>
		</xsl:when>
		 
		<xsl:otherwise>
			<xsl:if test='substring ($date, 9, 1) != ""'>
				<xsl:value-of select='substring ($date, 9, 2)'/><xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:otherwise>
		
	</xsl:choose>
	<xsl:value-of select='substring ($date, 1, 4)'/><xsl:text> </xsl:text>
	<xsl:value-of select='substring ($date, 11)'/>
</xsl:template>

<!--
	 table lookups
  -->
<xsl:template name='provider_type_cd'>
	<xsl:param name='type_cd'/>
	<xsl:choose>
		<xsl:when test='$type_cd="CON"'>
			<xsl:text>Consultant</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="PRISURG"'>
			<xsl:text>Primary surgeon</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="FASST"'>
			<xsl:text>First assistant</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="SASST"'>
			<xsl:text>Second assistant</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="SNRS"'>
			<xsl:text>Scrub nurse</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="TASST"'>
			<xsl:text>Third assistant</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="NASST"'>
			<xsl:text>Nurse assistant</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="ANEST"'>
			<xsl:text>Anesthetist</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="ANRS"'>
			<xsl:text>Anesthesia nurse</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="MDWF"'>
			<xsl:text>Midwife</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="ATTPHYS"'>
			<xsl:text>Attending physician</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="ADMPHYS"'>
			<xsl:text>Admitting physician</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="DISPHYS"'>
			<xsl:text>Discharging physician</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="RNDPHYS"'>
			<xsl:text>Rounding physician</xsl:text>
		</xsl:when>
		<xsl:when test='$type_cd="PCP"'>
			<xsl:text>Primary care provider</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select='$type_cd'/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name='administrative_gender_cd'>
	<xsl:param name='gender_cd'/>
	<xsl:choose>
		<xsl:when test='$gender_cd="M"'>
			<xsl:text>Male</xsl:text>
		</xsl:when>
		<xsl:when test='$gender_cd="F"'>
			<xsl:text>Female</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select='$gender_cd'/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>
 
