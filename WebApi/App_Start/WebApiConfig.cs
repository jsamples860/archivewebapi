﻿using System.Web.Http;
using Owin;
//this is the one that works
namespace WebApi
{
    public class WebApiConfig
    {
        public static void Configure(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new {id = RouteParameter.Optional});

            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always; // Add this line to enable detail mode in release
            


            app.UseWebApi(config);
        }
    }
}