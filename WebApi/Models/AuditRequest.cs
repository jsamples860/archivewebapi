﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class AuditRequest
    {


        public string PatientId { get; set; }
      
        public string StartDate { get; set; }

       
        public string EndDate { get; set; }

     
        public string sortBy { get; set; }

        public string sortOrder { get; set; }

        public int currentPage { get; set; }

        public int rowsOnPage { get; set; }
    }
}