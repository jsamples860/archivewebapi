﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class Note
    {
        public string NoteText { get; set; }
        public string NoteType { get; set; }
        public string NoteSubType { get; set; }
        public string Date { get; set; }
    }
}