﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class StickyNote
    {
        public string CreateUserName { get; set; }
        public string LastChanged { get; set; }
        public int Sequence { get; set; }
        public string StickyNoteImage { get; set; }
    }
}
