﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class SearchCriteria
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string dateOfBirth { get; set; }
        public string patientId { get; set; }
    }

    public class NewUser
    {
        public string email { get; set; }
        public string password { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }

    }

    public class NewSSOUser
    {
        public string userName { get; set; }
        public string athenaContext { get; set; }
        public string EHRSiteNumber { get; set; }
    }
}