﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class ARDetail
    {
        public string layer { get; set; }
        public string postingDate { get; set; }
        public string serviceDate { get; set; }

        public string user { get; set; }
        public string description { get; set; }
        public string amount { get; set; }

        public string balance { get; set; }
    }
}