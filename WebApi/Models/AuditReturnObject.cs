﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class AuditReturnObject
    {
        public int count { get; set; }
        public List<Greenway.PrimeSuite.DataContracts.AuditContract.Audit> logs { get; set; }

    }
}