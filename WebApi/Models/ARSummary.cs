﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class ARSummary
    {
        public string patientBalance { get; set; }
        public string insuranceBalance { get; set; }

        public string creditBalance { get; set; }

        public string patientCredit { get; set; }

        public string insuranceCredit { get; set; }

        public string undeterminedCredit { get; set; }
        public string prepayCredit { get; set; }
        public string claimsSuspendedSum { get; set; }
        public string claimsSubmittedSum { get; set; }
        public string suspendedARAmount { get; set; }

        public string patientLastStatementDate { get; set; }
        public string patientLastStatementAmount { get; set; }
        public string patientLastPaymentDate { get; set; }
        public string patientLastPaymentAmount { get; set; }

        public string lastVisitDate { get; set; }

        public string lastVisitAmount { get; set; }

        public string collectionBalance { get; set; }
    }
}