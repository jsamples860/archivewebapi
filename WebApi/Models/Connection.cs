﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class ConnectionModel
    {
        public string Database { get; set; }
        public string siteid { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Server { get; set; }
    }
}