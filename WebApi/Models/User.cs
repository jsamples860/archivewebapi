﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class User
    {
        public int expiresIn { get; set; }

        public string auth0ID { get; set; }
        public int Id { get; set; }
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public int Age { get; set; }
        public string siteID { get; set; }
        public string access_token { get; set; }
        public string id_token { get;  set; }
        public string token_type { get;  set; }
    }
}