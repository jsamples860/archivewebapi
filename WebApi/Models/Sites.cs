﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class Site
    {
        public string siteId { get; set; }
        public string siteName { get; set; }

        public ehrConnection dbConnection { get; set; }

    }

    public class ehrConnection
    {
        //here for legacy support
        public string siteid { get; set; }
        public string Server { get; set; }

        public string Database { get; set; }

        public string User { get; set; }

        public string Password { get; set; }
    }
}