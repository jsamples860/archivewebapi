﻿using Auth0.AuthenticationApi;
using Auth0.ManagementApi;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using WebApi.Models;


namespace WebApi.Helpers
{
    public class Audit
    {
        public static async void Log(string siteID, string patientID, string area, String data, System.Security.Claims.ClaimsPrincipal oUser)
        {
            try
            {
                string userID = CommonFunctions.GetUserId(oUser);

                Auth0.ManagementApi.Models.User user = await CommonFunctions.GetUser(userID);

                AddLogData(siteID, data, patientID, area, user.Email);
            }
            catch (Exception ex)
            {
                //don't throw it will create a loop of logging.
                //throw;
            }

        }

        public static void AddLogData(string siteID, string logMessage, string patientID, string area, string user)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ArchiveDB"].ConnectionString))
            {
                using (var SqlCmd = new SqlCommand("AddToAuditLog", conn))
                {
                    try
                    {

                        SqlCmd.CommandType = CommandType.StoredProcedure;
                        SqlCmd.Parameters.Add("@siteid", SqlDbType.NVarChar).Value = siteID;
                        SqlCmd.Parameters.Add("@log", SqlDbType.NVarChar).Value = logMessage;
                        SqlCmd.Parameters.Add("@patientID", SqlDbType.NVarChar).Value = patientID;
                        SqlCmd.Parameters.Add("@area", SqlDbType.NVarChar).Value = area;
                        SqlCmd.Parameters.Add("@date", SqlDbType.DateTime).Value = DateTime.UtcNow;
                        SqlCmd.Parameters.Add("@user", SqlDbType.NVarChar).Value = user;

                        conn.Open();
                        SqlCmd.ExecuteNonQuery();
                    }   // end try

                    catch (Exception ex)
                    {

                       
                    }
                }
            }
        }
    }
}
