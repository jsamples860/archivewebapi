﻿using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using Auth0.ManagementApi;
using Auth0.ManagementApi.Models;
using CodeHive.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace WebApi.Helpers
{
    public static class LoginHelper
    {
        public static async Task<WebApi.Models.User> AuthenticateUser(WebApi.Models.User user)
        {

            var client = new AuthenticationApiClient("blueridge.auth0.com");
            ResourceOwnerTokenRequest myRequest = new ResourceOwnerTokenRequest();
            myRequest.ClientId = "SYwkCk2o0PY1fD9FeqoOKbi0aIQuEKNu";
            myRequest.ClientSecret = "FTZI1p43U1vsao-M4jMrgnOpadt4noNmQ1Z_UTAD_hEQblJeCoLGal0maUBrd19L";
            myRequest.Password = "WeAreOneRomeStreet!ThisIsMyReallyLongPassword3141527";
            myRequest.Username = user.Username;
            myRequest.Scope = "openid profile full_access";
            myRequest.Realm = "SSODB";
            myRequest.Audience = "https://brarchiveapi.azurewebsites.net/";




            var response = await client.GetTokenAsync(myRequest);
            user.access_token = response.AccessToken;
            user.id_token = response.IdToken;
            user.expiresIn = response.ExpiresIn;
            user.token_type = response.TokenType;

            //set userinfo
            ManagementApiClient managementClient = Helpers.CommonFunctions.GetManagementClient();

            
            //var userid = Helpers.CommonFunctions.GetUserId((System.Security.Claims.ClaimsPrincipal)User);

        


           

            var userInfo = await client.GetUserInfoAsync(user.access_token);
            user.siteID = GetSSOUserSiteID(user.Username);
            user.auth0ID = userInfo.UserId;
            Auth0.ManagementApi.Models.UserUpdateRequest updateRequest = new Auth0.ManagementApi.Models.UserUpdateRequest();
            updateRequest.AppMetadata = new Dictionary<string, string>
                   {
                       {"siteID", user.siteID},
                        {"systemAdmin","false" },
                        {"role","user" }
                   };

            await managementClient.Users.UpdateAsync(userInfo.UserId, updateRequest);


            //     user.siteID = response.
            return user;

        }

        public static string  GetSSOUserSiteID(string username)
        {
            string siteID = "";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SSODB"].ConnectionString))
            {
                using (var SqlCmd = new SqlCommand("select * from Users where email=@email", conn))
                {
                    try
                    {

                        SqlCmd.CommandType = CommandType.Text;
                        SqlCmd.Parameters.AddWithValue("@email", username);

                        conn.Open();
                        var rdr = SqlCmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                siteID = rdr.GetString(rdr.GetOrdinal("siteId"));
                            
                            }
                        }
                        conn.Close();
                    }   // end try

                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    return siteID;
                }
            }
        }
    }



}