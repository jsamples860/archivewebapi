﻿using Auth0.AuthenticationApi;
using Auth0.ManagementApi;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Helpers
{
    public static class CommonFunctions
    {

        public static ManagementApiClient GetManagementClient()
        {
            var client = new RestClient("https://blueridge.auth0.com/oauth/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"grant_type\":\"client_credentials\",\"client_id\": \"VPOAb5uOXH9omXmmZuUIZc8OTQ4BFMRW\",\"client_secret\": \"KCb9df2Kb605YjjVB4DDtPSpgPYEQevcmU1K_8AXXBhcvTgyTIxO803nJkdT5EW_\",\"audience\": \"https://blueridge.auth0.com/api/v2/\"}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            tokenResponse tokenData = JsonConvert.DeserializeObject<tokenResponse>(response.Content);

            return new ManagementApiClient(tokenData.access_token, new Uri("https://blueridge.auth0.com/api/v2"));

        }

        public async static System.Threading.Tasks.Task<string> GetSiteID(System.Security.Claims.ClaimsPrincipal oUser)
        {
            string userID = GetUserId(oUser);

            Auth0.ManagementApi.Models.User user = await GetUser(userID);

            return ((string)user.AppMetadata["siteID"]);
           
        }


        public async static Task<int> GetSiteIDAsInt(System.Security.Claims.ClaimsPrincipal oUser)
        {
            string siteID = await GetSiteID(oUser);

            return int.Parse(siteID);
        }

        public static string GetUserId(System.Security.Claims.ClaimsPrincipal oUser)
        {
         
            string userId = oUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            
            return userId;
        }

        public async static System.Threading.Tasks.Task<Auth0.ManagementApi.Models.User> GetUser(string sID)
        {
            var managementClient = GetManagementClient();

             Auth0.ManagementApi.Models.User user = await managementClient.Users.GetAsync(sID);

            return user;
        }

        public async static System.Threading.Tasks.Task<bool>  IsUserAnAdmin(System.Security.Claims.ClaimsPrincipal oUser)
        {
            string userID = GetUserId(oUser);

            Auth0.ManagementApi.Models.User user = await GetUser(userID);

            if (((string)user.AppMetadata["role"]).ToLower() == "admin")
            {
                return true;
            }
            else
            {
                return false;
            }


          
        }


        public async static System.Threading.Tasks.Task<bool> IsSystemAdmin(System.Security.Claims.ClaimsPrincipal oUser)
        {
            string userID = GetUserId(oUser);

            Auth0.ManagementApi.Models.User user = await GetUser(userID);

            if (user.AppMetadata["systemAdmin"] != null && ((string)user.AppMetadata["systemAdmin"]).ToLower() == "true")
            {
                return true;
            }
            else
            {
                return false;
            }



        }


    }
}