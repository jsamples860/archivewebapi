﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using ICSharpCode;
using ICSharpCode.SharpZipLib.Zip.Compression;

namespace WebApi.Helpers
{
    public static class SharpZipHelper
    {
        private const int BUFFERSIZE = 8192;

        public static string UnzipString(byte[] arrByte)
        {
            MemoryStream memoryStream1 = new MemoryStream();
            MemoryStream memoryStream2 = new MemoryStream();
            BinaryWriter binaryWriter = new BinaryWriter((Stream)memoryStream1);
            BinaryReader binaryReader = new BinaryReader((Stream)memoryStream2);
            binaryWriter.Write(arrByte);
            memoryStream1.Position = 0L;
            UnCompress((Stream)memoryStream1, (Stream)memoryStream2);
            memoryStream2.Position = 0L;
            return Encoding.GetEncoding(1252).GetString(binaryReader.ReadBytes(checked((int)memoryStream2.Length)));
        }

        public static void Compress(Stream stIn, Stream stOut)
        {
            Deflater deflater = new Deflater();
            byte[] numArray1 = new byte[8192];
            byte[] numArray2 = new byte[8192];
            if (stIn == null || stOut == null)
                return;
            label_9:
            int len = stIn.Read(numArray1, 0, 8192);
            if (len == 0)
            {
                deflater.Finish();
                while (!deflater.IsFinished)
                {
                    int count = deflater.Deflate(numArray2, 0, 8192);
                    if (count != 0)
                        stOut.Write(numArray2, 0, count);
                    else
                        break;
                }
            }
            else
            {
                deflater.SetInput(numArray1, 0, len);
                while (!deflater.IsNeedingInput)
                {
                    int count = deflater.Deflate(numArray2, 0, 8192);
                    stOut.Write(numArray2, 0, count);
                }
                goto label_9;
            }
        }

        public static void UnCompress(Stream stIn, Stream stOut)
        {
            Inflater inflater = new Inflater();
            byte[] buffer = new byte[8192];
            byte[] numArray = new byte[8192];
            if (stIn == null || stOut == null)
                return;
            int length;
            do
            {
                length = stIn.Read(buffer, 0, 8192);
                inflater.SetInput(buffer, 0, length);
                do
                {
                    int count = inflater.Inflate(numArray, 0, 8192);
                    if (count != 0)
                        stOut.Write(numArray, 0, count);
                }
                while (!inflater.IsFinished && !inflater.IsNeedingInput);
            }
            while (length != 0);
        }
    }

}