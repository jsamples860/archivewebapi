﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApi.Models;
using BCrypt.Net;
using System.Threading.Tasks;

namespace WebApi.Helpers
{
    public class SSOUserHelper
    {

        public async static Task<bool> AddSSOUserToDB(NewSSOUser user)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SSODB"].ConnectionString))
            {
                using (var SqlCmd = new SqlCommand("INSERT INTO dbo.Users (Email,Password, SiteID) values(@Email,  @Password, @SiteID)", conn))
                {
                    try
                    {
                        var email = user.userName + "@AthenaPractice_" + user.athenaContext + ".SSO";
                        string passwordHash = BCrypt.Net.BCrypt.HashPassword("WeAreOneRomeStreet!ThisIsMyReallyLongPassword3141527",10);
                        
                        SqlCmd.CommandType = CommandType.Text;
                        SqlCmd.Parameters.Add("@Email", SqlDbType.NVarChar).Value = email;
                        SqlCmd.Parameters.Add("@Password", SqlDbType.NVarChar).Value = passwordHash;
                        SqlCmd.Parameters.Add("@SiteID", SqlDbType.NVarChar).Value = user.EHRSiteNumber;

                        conn.Open();
                        SqlCmd.ExecuteNonQuery();
                        User inUser = new User();
                        inUser.Username = email;
                        await LoginHelper.AuthenticateUser(inUser);
                        return true;
                    }   // end try


                    catch (Exception ex)
                    {
                       
                        throw ex;
                    }
                    return false;
                }
            }
        }

        public  static bool DeleteSSOUserFromDBIfExists(string userID)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SSODB"].ConnectionString))
            {
                using (var SqlCmd = new SqlCommand("Delete from dbo.Users where Id = @id", conn))
                {
                    try
                    {
                        
                        SqlCmd.CommandType = CommandType.Text;
                        SqlCmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = userID;
                        

                        conn.Open();
                        SqlCmd.ExecuteNonQuery();
                        User inUser = new User();
                       
                        return true;
                    }   // end try


                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    return false;
                }
            }
        }

    }
}