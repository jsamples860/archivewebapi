﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using WebApi.Models;

namespace WebApi.Helpers
{
    public class SiteHelper
    {
        public static void DeleteSite(String siteID)
        {

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ArchiveDB"].ConnectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = conn;
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = "Delete from RO_AllSites where siteID = @siteID";

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@siteID";
                param.Value = siteID;
                command.Parameters.Add(param);

                command.ExecuteNonQuery();

                conn.Close();

            }
        }

        public static List<Site> EditSite(Site editSite)
        {

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ArchiveDB"].ConnectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = conn;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = @"Update RO_AllSites 
                                        set siteName = @siteName, 
                                        ConnectionString=@ConnectionString 
                                        where siteId=@siteId";

                SqlParameter param3 = new SqlParameter();
                param3.ParameterName = "@siteName";
                param3.Value = editSite.siteName;
                command.Parameters.Add(param3);

                editSite.dbConnection.siteid = editSite.siteId;
                SqlParameter param2 = new SqlParameter();
                param2.ParameterName = "@ConnectionString";
                param2.Value = JsonConvert.SerializeObject(editSite.dbConnection);
                command.Parameters.Add(param2);

                SqlParameter param1 = new SqlParameter();
                param1.ParameterName = "@siteId";
                param1.Value = JsonConvert.SerializeObject(editSite.siteId);
                command.Parameters.Add(param1);


                command.ExecuteNonQuery();

                conn.Close();


            }

            return GetAllSites();
        }

        public static List<Site> AddSite(Site newSite)
        {
            
             using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ArchiveDB"].ConnectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = conn;
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = @"Insert into RO_AllSites (siteId,ConnectionString,siteName) 
                                        values(@siteID, @ConnectionString, @siteName)";

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@siteID";
                param.Value = newSite.siteId;
                command.Parameters.Add(param);

                newSite.dbConnection.siteid = newSite.siteId;
                SqlParameter param2 = new SqlParameter();
                param2.ParameterName = "@ConnectionString";
                param2.Value = JsonConvert.SerializeObject(newSite.dbConnection);
                command.Parameters.Add(param2);

                SqlParameter param3 = new SqlParameter();
                param3.ParameterName = "@siteName";
                param3.Value = newSite.siteName;
                command.Parameters.Add(param3);


                command.ExecuteNonQuery();

                conn.Close();

               
            }

            return GetAllSites();
        }
        public static List<Site> GetAllSites()
        {
            List<Site> sites = new List<Site>();

            StringBuilder strJson = new StringBuilder("");
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ArchiveDB"].ConnectionString))
            {
                using (var SqlCmd = new SqlCommand("select * from RO_AllSites", conn))
                {
                    try
                    {

                        SqlCmd.CommandType = CommandType.Text;
                      
                        conn.Open();
                        var rdr = SqlCmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                Site theSite = new Site();
                                theSite.siteId  = rdr.GetString(rdr.GetOrdinal("siteId"));
                                theSite.siteName = rdr.GetString(rdr.GetOrdinal("siteName"));
                                theSite.dbConnection = JsonConvert.DeserializeObject<ehrConnection>(rdr.GetString(rdr.GetOrdinal("ConnectionString")));
 
                                sites.Add(theSite);
                            }
                        }
                        conn.Close();
                    }   // end try

                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    return sites;
                }
            }
        }
    }
}