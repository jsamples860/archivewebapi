﻿using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Configuration;
using iAuth2._0.Models;
using System.Collections.Generic;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography.X509Certificates;

namespace iAuth2._0
{
    public class AssertionConsumerHelper
    {

        public static AssertionData ProcessRequest(string context)
        {

            // pull Base 64 encoded XML saml assertion from Request and decode it

            string decodedContent = Uri.UnescapeDataString(context);
            XmlDocument SAMLXML = new XmlDocument();
            String SAMLResponseString = System.Text.Encoding.UTF8.GetString(
                Convert.FromBase64String(decodedContent.ToString()));
            SAMLXML.LoadXml(SAMLResponseString);

            AssertionData SSOData = null;
            // Finding 
            AssertionType assertion = GetAssertionFromXMLDoc(SAMLXML);

            if (assertion.Issuer.Value == "https://athenanet.athenahealth.com")
            {
                string name = ((iAuth2._0.Models.NameIDType)assertion.Subject.Items[0]).Value;
                SSOData = new AssertionData(assertion);
                SSOData.SAMLAttributes.Add("name", name);

            }
            return SSOData;

        }

        public static AssertionType GetAssertionFromXMLDoc(XmlDocument SAMLXML)
        {
            XmlNamespaceManager ns = new XmlNamespaceManager(SAMLXML.NameTable);
            ns.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");
            XmlElement xeAssertion = SAMLXML.DocumentElement.SelectSingleNode("saml:Assertion", ns) as XmlElement;

            XmlSerializer serializer = new XmlSerializer(typeof(AssertionType));

            AssertionType assertion = (AssertionType)serializer.Deserialize(new XmlNodeReader(xeAssertion));

            return assertion;
        }

        public static bool ValidateX509CertificateSignature(XmlDocument SAMLResponse)
        {
            XmlNodeList XMLSignatures = SAMLResponse.GetElementsByTagName("Signature", "http://www.w3.org/2000/09/xmldsig#");

            // Checking If the Response or the Assertion has been signed once and only once.
            if (XMLSignatures.Count != 1) return false;
            SignedXml SignedSAML = new SignedXml(SAMLResponse);
            SignedSAML.LoadXml((XmlElement)XMLSignatures[0]);

            String CertPath = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/CoverMyMeds.cer");
            X509Certificate2 SigningCert = new X509Certificate2(CertPath);

            return SignedSAML.CheckSignature(SigningCert, true);
        }
    }
}
