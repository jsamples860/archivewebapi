﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using WebApi.Models;

namespace WebApi.Helpers
{
    public class PatientHelper
    {
        private static string BuildConnectionString(string database,string userName, string userPassword, string server)
        {
            string connectString = "";
           
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectString);

            builder.DataSource = server;
            builder.UserID = userName;
            builder.Password = userPassword;
            builder.ConnectTimeout = 60;
            builder.InitialCatalog = database;

            return builder.ConnectionString;
        }

        private static string GetConnectionString(int  siteID)
        {
            try
            {
                string connectionString = "";
                var ConnModel = new ConnectionModel();
              
                string connectionStringFromMaster = ConfigurationManager.ConnectionStrings["ArchiveDB"].ConnectionString;

                if (connectionStringFromMaster == null)
                    throw new Exception("Connection String was not passed");
                using (var connection = new SqlConnection())
                {

                    connection.ConnectionString = connectionStringFromMaster;
                    connection.Open();

                    var cmd = new SqlCommand("select ConnectionString from RO_AllSites where siteId = @siteId",
                        connection);

                    var param = new SqlParameter
                    {
                        ParameterName = "@siteId",
                        Value = siteID
                    };

                    cmd.Parameters.Add(param);
                    var rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        var str1 = rdr["ConnectionString"].ToString();
                        ConnModel = JsonConvert.DeserializeObject<ConnectionModel>(str1);
                        connectionString = BuildConnectionString(ConnModel.Database, ConnModel.User, ConnModel.Password, ConnModel.Server);
                       
                    }

                }
                return connectionString;
            }
            catch (Exception ex)
            {
                throw;
            }

        }



        static public List<Note> GetAccountNotes(int patientID, int siteID)
        {


            StringBuilder strJson = new StringBuilder("");

            string queryText = @"select 
	                             Convert(varchar,LastModifiedDate,101) as LastModifiedDate
                                  ,Text
	                            ,convert(varchar,NT.NoteType) as NoteType
                                ,convert(varchar,nts.NoteType) as NoteSubType
                              FROM [Notes]
                              join NoteType NT on NT.NoteTypeID = Notes.NoteTypeID
                              join notetype nts on nts.NoteTypeID = notes.NoteSubType
                              where patientid = @patientID 
                              and not UserID in (0,1)
                              order by Convert(date,LastModifiedDate) desc";
            List<Note> notes = new List<Note>();

            try
            {
                using (SqlConnection conn = new SqlConnection(GetConnectionString(siteID)))
                {
                    conn.Open();

                    SqlCommand command = new SqlCommand();
                    command.Connection = conn;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = queryText;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@patientID";
                    param.Value = patientID;

                    command.Parameters.Add(param);
                    var rdr = command.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            Note item = new Note();
                            item.NoteText = rdr["Text"].ToString();
                            item.NoteType = rdr["NoteType"].ToString();
                            item.NoteSubType = rdr["NoteSubType"].ToString();
                            item.Date = rdr["LastModifiedDate"].ToString();
                            notes.Add(item);
                        }
                    }


                    conn.Close();
                }//end using
            }
            catch (Exception ex)
            {

                throw;

            }

            return notes;


        }


        static public List<StickyNote> GetPatientStickNotes(int patientID, int siteID)
        {


            StringBuilder strJson = new StringBuilder("");

            string queryText = @"
SET NOCOUNT ON
DECLARE @tot int

DECLARE @stickynote table(
	seqid	numeric identity(1,1)
	,stickynotebinid numeric
)
SELECT @tot=count(*)
FROM StickyNoteBin
WHERE PatientID = @patid

INSERT INTO @stickynote(stickynotebinid)
SELECT StickyNoteBinID
FROM StickyNoteBin
WHERE PatientID = @patid
ORDER BY CreateDate desc

SET NOCOUNT OFF

	SELECT	StickyNoteBinID		= b.StickyNoteBinID
		,CreateUserID		= b.CreateUserID
		,CreateUserName		= isNULL(cu.UserName,'')
		,CreateDate		= convert(varchar(10),dbo.iGMTFormatLongDate(CreateDate, 'mm/dd/yyyy'))
		,LastChangedUserID	= b.LastChangedUserID
		,LastChangedUserName	= isNULL(lc.UserName,'')
		,LastChanged		= convert(varchar(10),dbo.iGMTFormatLongDate(LastChanged, 'mm/dd/yyyy'))
		--,Sequence		= convert(varchar(18),seqid) + ' of ' + convert(varchar(18),@tot)
		,Sequence		= t.seqid
		,TotalCount		= @tot
		,StickyNoteImage	= b.StickyNoteImage
	FROM @stickynote t
	JOIN StickyNoteBin b WITH (NOLOCK) on t.StickyNoteBinID = b.StickyNoteBinID
	JOIN Users cu WITH (NOLOCK) on b.CreateUserID = cu.UserID
	JOIN Users lc WITH (NOLOCK) on b.LastChangedUserID = lc.UserID
	Order By seqid";
            List<StickyNote> notes = new List<StickyNote>();

            try
            {
                using (SqlConnection conn = new SqlConnection(GetConnectionString(siteID)))
                {
                    conn.Open();

                    SqlCommand command = new SqlCommand();
                    command.Connection = conn;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = queryText;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@patid";
                    param.Value = patientID;

                    command.Parameters.Add(param);
                    var rdr = command.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            StickyNote item = new StickyNote();
                            item.CreateUserName = rdr["CreateUserName"].ToString();
                            item.LastChanged = rdr["LastChanged"].ToString();
                           
                            item.StickyNoteImage = WebApi.Helpers.SharpZipHelper.UnzipString((byte[])rdr["StickyNoteImage"]).Replace("\0", "");
                            notes.Add(item);
                        }
                    }


                    conn.Close();
                }//end using
            }
            catch (Exception ex)
            {

                throw;

            }

            return notes;


        }



        static public ARSummary GetARHeader(int patientID, int siteID)
        {


            StringBuilder strJson = new StringBuilder("");

            string queryText = @"spRPTAccountSummary";

            ARSummary item = new ARSummary();
            try
            {
                using (SqlConnection conn = new SqlConnection(GetConnectionString(siteID)))
                {
                    conn.Open();

                    SqlCommand command = new SqlCommand();
                    command.Connection = conn;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = queryText;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@PatientID";
                    param.Value = patientID;

                    command.Parameters.Add(param);
                    var rdr = command.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                           
                            item.patientBalance = rdr["patientBalance"].ToString();
                            item.insuranceBalance = rdr["insuranceBalance"].ToString();
                            item.creditBalance = rdr["CreditBalance"].ToString();
                            item.patientCredit = rdr["PatientCredit"].ToString();
                            item.insuranceCredit = rdr["InsuranceCredit"].ToString();
                            item.undeterminedCredit = rdr["UndeterminedCredit"].ToString();
                            item.prepayCredit = rdr["PrepayCredit"].ToString();
                            item.claimsSuspendedSum = rdr["ClaimsSuspendedSum"].ToString();
                            item.claimsSubmittedSum = rdr["ClaimsSubmittedSum"].ToString();
                            item.patientLastStatementDate = rdr["PatientLastStatementDate"].ToString();
                            item.patientLastStatementAmount = rdr["PatientLastStatementAmount"].ToString();
                            item.patientLastPaymentDate = rdr["PatientLastPaymentDate"].ToString();
                            item.patientLastPaymentAmount = rdr["PatientLastPaymentAmount"].ToString();
                            item.lastVisitAmount = rdr["LastVisitAmount"].ToString();
                            item.lastVisitDate = rdr["LastVisitDate"].ToString();
                            item.collectionBalance = rdr["CollectionBalance"].ToString();
                        }
                    }


                    conn.Close();
                }//end using
            }
            catch (Exception ex)
            {

                throw;

            }

            return item;


        }


        static public List<ARDetail> GetARDetails(int patientID, int siteID)
        {

            
            StringBuilder strJson = new StringBuilder("");

            string queryText = $@"declare @hdoc int exec sp_xml_preparedocument @hDOC output, '<criteria PatientID=""{patientID.ToString()}"" TransLimit=""63"" GroupType=""0"" ShowNonPrint=""0"" BillableProviderID=""-1"" RenderingProviderID=""-1"" PracticeLocationID=""-1"" ServiceLocationID=""-1""  EnhanceDescription=""0"" ShowSuspended=""1"" PrintNPI=""0"" ShowServLoc=""0"" ShowChildren=""1"" StartDate=""01/01/1990""  EndDate=""01/01/2050""   />' exec rptAccountDetailsGetDetails @hDoc exec sp_xml_removedocument @hDOC";
            List<ARDetail> details = new List<ARDetail>();

            try
            {
                using (SqlConnection conn = new SqlConnection(GetConnectionString(siteID)))
                {
                    conn.Open();

                    SqlCommand command = new SqlCommand();
                    command.Connection = conn;
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = queryText;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@patientID";
                    param.Value = patientID;

                    command.Parameters.Add(param);
                    var rdr = command.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            ARDetail item = new ARDetail();
                            item.layer = rdr["Layer"].ToString();
                            item.postingDate = rdr["PostingDate"].ToString();
                            item.serviceDate = rdr["ServiceDate"].ToString();
                            item.user = rdr["Initials"].ToString();
                            item.description = rdr["Description"].ToString();
                            item.amount = rdr["Total"].ToString();
                            item.balance = rdr["LineBalance"].ToString();
                            details.Add(item);
                        }
                    }


                    conn.Close();
                }//end using
            }
            catch (Exception ex)
            {

                throw new Exception(queryText);

            }

            return details;


        }


    }
}