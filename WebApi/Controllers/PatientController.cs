﻿using Auth0.AuthenticationApi;
using Auth0.ManagementApi;
using CodeHive.Models.PrimeSuite;
using CodeHive.Models.PrimeSuite.Requests;
using CodeHive.Models.PrimeSuite.Responses;
using Greenway.PrimeSuite.DataContracts.Person.Patient;
using Greenway.PrimeSuite.DataContracts.Person.Patient.Visit;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using WebApi.Helpers;
using WebApi.Models;


namespace WebApi.Controllers
{
    public class PatientController : ApiController
    {

        //Get Audit Log 
        [Authorize]
        [HttpPost, Route("api/audit")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetAuditLog(WebApi.Models.AuditRequest req)
        {

            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);

            var caller = new CodeHive.Services.GeneralService();
            var allLogs = new Greenway.PrimeSuite.DataContracts.AuditContract.AuditResponse();
            CodeHive.Models.PrimeSuite.Requests.AuditRequest oGWayRequest = new CodeHive.Models.PrimeSuite.Requests.AuditRequest();
            oGWayRequest.EndDate = req.EndDate;
            oGWayRequest.StartDate = req.StartDate;
            oGWayRequest.PatientId = req.PatientId;

            allLogs = caller.AuditListGet(oGWayRequest,  siteID);


            var logList = allLogs.Audits.Skip(req.currentPage * req.rowsOnPage).Take(req.rowsOnPage).ToList();
            AuditReturnObject returns = new AuditReturnObject();
            returns.count = allLogs.Audits.Count;
            returns.logs = logList;
            return Json(returns);
        }

        //Get Document 
        [Authorize]
        [HttpGet, Route("api/document/{documentid}/{seqNumber}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetDocument(string documentid,string seqNumber)
        {
            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);

            Helpers.Audit.Log(siteID.ToString(), "", "Documents", "Viewed document: " + documentid, (System.Security.Claims.ClaimsPrincipal)User);

           
            var caller = new CodeHive.Services.DocumentService();
            var returns = new DocumentGetResponse();
            returns = caller.GetDocument(int.Parse(documentid), int.Parse(seqNumber), siteID);

            return Json(returns);
        }

        //Print Whole Chart 
        [Authorize]
        [HttpGet, Route("api/printchart/{patientid}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> PrintChart(string patientid)
        {
            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);

            Helpers.Audit.Log(siteID.ToString(),"", "Documents", "Print Chart: " + patientid, (System.Security.Claims.ClaimsPrincipal)User);

            

            PrintChartResponse returns = new PrintChartResponse();

            Greenway.PrintChart ChartPrinter = new Greenway.PrintChart(0);
            ChartPrinter.ResetPDFGenerator();
            byte[] pdf = ChartPrinter.PrintPatientChart(siteID, patientid);
            ChartPrinter.CleanUp();

            returns.BaseImage64 = Convert.ToBase64String(pdf);

            return Json(returns);
        }



        //Get Document List 
        [Authorize]
        [HttpGet, Route("api/documentlist/{patientid}/{pageSize}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetDocumentList(string patientid,int pageSize = 900)
        {

            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);

            var caller = new CodeHive.Services.DocumentService();
            var returns = new  List<DocumentListItem>();
            returns = caller.GetDocumentList(int.Parse(patientid),900, siteID);

            return Json(returns);
        }

        //Get Insurance 
         [Authorize]
        [HttpGet, Route("api/patient/insurance/{patientid}/")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetInsurance(string patientid)
        {

            ////lookup siteID
            string siteID = await Helpers.CommonFunctions.GetSiteID((System.Security.Claims.ClaimsPrincipal)User);

            var caller = new CodeHive.Services.PatientService();
            var returns = new InsuranceGetResponse();
            returns = caller.GetPatientInsurance(patientid, siteID,"PrimeSuite");

            return Json(returns);
        }

        //Patient Search 
         [Authorize]
        [HttpPost, Route("api/patient/search")]
        public async System.Threading.Tasks.Task<IHttpActionResult> PatientSearch(SearchCriteria criteria)
        {
            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);


            Helpers.Audit.Log(siteID.ToString(), "", "Patient Search", "Searched for PatientID: " + criteria.patientId + " FirstName: " + criteria.firstName + " LastName: " + criteria.lastName + " DOB: " + criteria.dateOfBirth, (System.Security.Claims.ClaimsPrincipal)User);

           
            int patientID = 0;
            int.TryParse(criteria.patientId, out patientID);
            

            var caller = new CodeHive.Services.PatientService();
            var returns = new CodeHive.Models.PrimeSuite.Responses.PatientSearchResponse();
            returns = caller.PatientSearch(patientID, criteria.firstName,criteria.dateOfBirth, criteria.lastName, siteID, "PrimeSuite");
            
            return Json(returns);
        }


        // GET: Patient
        [Authorize]
        [HttpGet, Route("api/patient/chart/{patientid}")]
        public async  System.Threading.Tasks.Task<IHttpActionResult> GetChart(string patientid)
        {
            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);


            Helpers.Audit.Log(siteID.ToString(), "", "Viewed Patient", "Viewed record for PatientID: " + patientid, (System.Security.Claims.ClaimsPrincipal)User);


          

            PatientChartResponse returns = new PatientChartResponse();
            CodeHive.Services.PatientService oCaller = new CodeHive.Services.PatientService();

            var filter = new ChartFilters();
            filter.Allergy = true;
            filter.FamilyMedicalHistory = true;
            filter.FlowSheet = true;
            filter.GeneticHistory = true;
            filter.Immunizations = true;
            filter.Medications = true;
            filter.PastMedicalHistory = true;
            filter.PastSurgicalHistory = true;
            filter.ProblemListSort = true;
            filter.ReproductivityHistory = true;
            filter.SocialHistory = true;
            filter.VisitList = true;
            filter.Vitals = true;
            filter.Photo = true;
            
    

            returns = oCaller.GetDetails(patientid, filter, "0", siteID, "PrimeSuite");

            return Json(returns);
        }


        // GET: Demographics
        [Authorize]
        [HttpGet, Route("api/patient/visit/{visitid}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetVisitBillingData(string visitid)
        {
            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);
            VisitDetailsGetResponse returns;

            CodeHive.Services.PatientService oCaller = new CodeHive.Services.PatientService();
            returns =  oCaller.VisitDetailsGet(int.Parse(visitid), siteID, "PrimeSuite");

            return Json(returns);
        }


        // GET: Flags
        [Authorize]
        [HttpGet, Route("api/patient/flags/{patientid}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetFlags(string patientid)
        {

            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);


            CodeHive.Services.PatientService oCaller = new CodeHive.Services.PatientService();

            var returnData  = oCaller.PatientFlagGet(int.Parse(patientid), siteID);

            return Json(returnData);
        }


        [Authorize]
        [HttpGet, Route("api/patient/accountnotes/{patientid}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetAccountNotes(int patientid)
        {
            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);

            var returnData = PatientHelper.GetAccountNotes(patientid, siteID);

            return Json(returnData);
        }

        [Authorize]
        [HttpGet, Route("api/patient/stickynotes/{patientid}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetStickNotes(int patientid)
        {
            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);

            var returnData = PatientHelper.GetPatientStickNotes(patientid, siteID);

            return Json(returnData);
        }


        [Authorize]
        [HttpGet, Route("api/patient/arsummary/{patientid}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetARSummary(int patientid)
        {
            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);




            var returnData = PatientHelper.GetARHeader(patientid, siteID);

            return Json(returnData);
        }

        [Authorize]
        [HttpGet, Route("api/patient/ardetails/{patientid}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetARDetails(int patientid)
        {
            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);

            var returnData = PatientHelper.GetARDetails(patientid, siteID);

            return Json(returnData);
        }

        // GET: Flags
        [Authorize]
        [HttpGet, Route("api/patient/accountbalance/{patientid}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetAccountBalance(string patientid)
        {
            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);

            CodeHive.Services.PatientService oCaller = new CodeHive.Services.PatientService();

            var returnData = oCaller.PatientBalanceGet(int.Parse(patientid), siteID);

            return Json(returnData);
        }

        // GET: Demographics
        [Authorize]
        [HttpGet, Route("api/patient/demographics/{patientid}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetDemographics(string patientid)
        {

            //lookup siteID
            int siteID = await Helpers.CommonFunctions.GetSiteIDAsInt((System.Security.Claims.ClaimsPrincipal)User);


            DemographicsGetResponse returns = new DemographicsGetResponse();
            CodeHive.Services.PatientService oCaller = new CodeHive.Services.PatientService();

            var filter = new ChartFilters();
            filter.Allergy = true;
            filter.FamilyMedicalHistory = true;
            filter.FlowSheet = true;
            filter.GeneticHistory = true;
            filter.Immunizations = false;
            filter.Medications = true;
            filter.PastMedicalHistory = true;
            filter.PastSurgicalHistory = true;
            filter.ProblemListSort = true;
            filter.SocialHistory = true;
            filter.VisitList = true;
            filter.Vitals = true;

            
            returns = oCaller.PatientDetailDemographicGet(int.Parse(patientid), siteID, "PrimeSuite");

            return Json(returns);
        }
    }
}