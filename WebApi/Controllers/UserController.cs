﻿using Auth0.AuthenticationApi;
using Auth0.ManagementApi;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class UserController : ApiController
    {

       

        private AuthenticationApiClient GetAuthenticationAPIClient()
        {
         
            return new AuthenticationApiClient("blueridge.auth0.com");

        }


        [Authorize]
        [HttpGet, Route("api/user/Admin")]
        public async System.Threading.Tasks.Task<IHttpActionResult> IsUserAdmin()
        {
            bool bIsAdmin = await Helpers.CommonFunctions.IsUserAnAdmin((System.Security.Claims.ClaimsPrincipal)User);
            return Json(new{ result = bIsAdmin });
        }


        [Authorize]
        [HttpGet, Route("api/user/sysAdmin")]
        public async System.Threading.Tasks.Task<IHttpActionResult> IsUserSysAdmin()
        {
            bool bIsAdmin = await Helpers.CommonFunctions.IsSystemAdmin((System.Security.Claims.ClaimsPrincipal)User);
            return Json(new { result = bIsAdmin });
        }


        [Authorize]
        [HttpGet, Route("api/user/siteid")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetSiteID()
        {
            var managementClient = Helpers.CommonFunctions.GetManagementClient();

            string siteID = await Helpers.CommonFunctions.GetSiteID((System.Security.Claims.ClaimsPrincipal)User);

            return Json(new { siteID = siteID });
        }

        [Authorize]
        [HttpGet, Route("api/user/GetAll")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetAllUsersForPracticeAsync()
        {
           

            var managementClient = Helpers.CommonFunctions.GetManagementClient();

           string siteID = await Helpers.CommonFunctions.GetSiteID((System.Security.Claims.ClaimsPrincipal)User);

            bool bIsAdmin = await Helpers.CommonFunctions.IsUserAnAdmin((System.Security.Claims.ClaimsPrincipal) User);

            if (bIsAdmin)
            {
                Helpers.Audit.Log(siteID, "", "Viewed Users List", "Viewed list of all users", (System.Security.Claims.ClaimsPrincipal)User);

                Auth0.Core.Collections.IPagedList<Auth0.ManagementApi.Models.User> users =
                await managementClient.Users.GetAllAsync(0, 100,false, "email:1");

                var foundUsers = users.Where(u => u.AppMetadata?.siteID == siteID && u.Email != "jsamples@outlook.com").ToList();
                var currentPageUserCount = users.Count;
                var pageCount = 1;
                 while (currentPageUserCount == 100)
                 {
                    users = await managementClient.Users.GetAllAsync(pageCount, 100, false, "email:1");
                    currentPageUserCount = users.Count;
                    pageCount = pageCount + 1;

                    var found = users.Where(u => u.AppMetadata?.siteID == siteID && u.Email != "jsamples@outlook.com");
                    foreach (var item in found)
                    {
                        foundUsers.Add(item);
                    }
                   

                }

             
                return Json(foundUsers);
            }
            else
            {
                return Unauthorized();
            }

            
        }


        [Authorize]
        [HttpGet, Route("api/user/GetAll2")]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetAllUsersForPracticeAsync2()
        {


            var managementClient = Helpers.CommonFunctions.GetManagementClient();

            string siteID = await Helpers.CommonFunctions.GetSiteID((System.Security.Claims.ClaimsPrincipal)User);

            bool bIsAdmin = await Helpers.CommonFunctions.IsUserAnAdmin((System.Security.Claims.ClaimsPrincipal)User);

            if (bIsAdmin)
            {
                Helpers.Audit.Log(siteID, "", "Viewed Users List", "Viewed list of all users", (System.Security.Claims.ClaimsPrincipal)User);

                Auth0.Core.Collections.IPagedList<Auth0.ManagementApi.Models.User> users =
                await managementClient.Users.GetAllAsync(0, 100, false, "email:1",null, null, null, "app_metadata.siteID:\"" + siteID + "\"");

                var foundUsers = users.Where(u => u.AppMetadata?.siteID == siteID && u.Email != "jsamples@outlook.com").ToList();
                var currentPageUserCount = users.Count;
                var pageCount = 1;
                while (currentPageUserCount == 100)
                {
                    users = await managementClient.Users.GetAllAsync(pageCount, 100, false, "email:1",null,null,null, "app_metadata.siteID:\"" + siteID + "\"" );
                    currentPageUserCount = users.Count;
                    pageCount = pageCount + 1;

                    var found = users.Where(u => u.AppMetadata?.siteID == siteID && u.Email != "jsamples@outlook.com");
                    foreach (var item in found)
                    {
                        foundUsers.Add(item);
                    }


                }


                return Json(foundUsers);
            }
            else
            {
                return Unauthorized();
            }


        }


        [Authorize]
        [HttpPost, Route("api/user/AddUser")]
        public async System.Threading.Tasks.Task<IHttpActionResult> AddUser(NewUser newUser)
        {

            

            string siteID = await Helpers.CommonFunctions.GetSiteID((System.Security.Claims.ClaimsPrincipal)User);

            bool bIsAdmin = await Helpers.CommonFunctions.IsUserAnAdmin((System.Security.Claims.ClaimsPrincipal)User);

            if (bIsAdmin)
            {
                
                Helpers.Audit.Log(siteID, "", "Add User", "Added user: " + newUser.email, (System.Security.Claims.ClaimsPrincipal)User);

                ManagementApiClient managementClient = Helpers.CommonFunctions.GetManagementClient();
      
                Auth0.ManagementApi.Models.UserCreateRequest user = new Auth0.ManagementApi.Models.UserCreateRequest();


                user.Email = newUser.email;
                user.FirstName = newUser.firstname;
                user.LastName = newUser.lastname;
                user.Password = newUser.password;
                user.Connection = "Username-Password-Authentication";
               

                user.AppMetadata = new Dictionary<string, string>
                    {
                        {"siteID", siteID},
                        {"role", "user"}
                    };



                await managementClient.Users.CreateAsync(user);
                



                return Ok();
            }
            else
            {
                return Unauthorized();
            }


        }


        [Authorize]
        [HttpPost, Route("api/user/AddSSOUser")]
        public async System.Threading.Tasks.Task<IHttpActionResult> AddSSOUser(NewSSOUser newUser)
        {

            string siteID = await Helpers.CommonFunctions.GetSiteID((System.Security.Claims.ClaimsPrincipal)User);

            bool bIsAdmin = await Helpers.CommonFunctions.IsUserAnAdmin((System.Security.Claims.ClaimsPrincipal)User);

            newUser.EHRSiteNumber = siteID;
            if (bIsAdmin)
            {

                Task t =  Helpers.SSOUserHelper.AddSSOUserToDB(newUser);
                await t;
                return Ok();
            }
            else 
            {
                return Unauthorized();
            }

        }


        [Authorize]
        [HttpGet, Route("api/user/switch/{siteID}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> SwicthSite(String siteID)
        {
            bool bIsSysAdmin = await Helpers.CommonFunctions.IsSystemAdmin((System.Security.Claims.ClaimsPrincipal)User);
            
            if (bIsSysAdmin)
            {
                Helpers.Audit.Log(siteID, "", "Switch sites",User.Identity.Name + " switched to new site: " + siteID, (System.Security.Claims.ClaimsPrincipal)User);

                ManagementApiClient managementClient = Helpers.CommonFunctions.GetManagementClient();

                Auth0.ManagementApi.Models.UserUpdateRequest user = new Auth0.ManagementApi.Models.UserUpdateRequest();
                var userid = Helpers.CommonFunctions.GetUserId((System.Security.Claims.ClaimsPrincipal)User);
                
                user.AppMetadata = new Dictionary<string, string>
                    {
                        {"siteID", siteID},
                        {"systemAdmin","true" },
                        {"role","admin" }
                    };



                await managementClient.Users.UpdateAsync(userid, user);




                return Ok();
            }
            else
            {
                return Unauthorized();
            }


        }



        [Authorize]
        [HttpGet, Route("api/user/makesiteadmin/{userID}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> MakeSiteAdmin(String userID)
        {
            bool bIsAdmin = await Helpers.CommonFunctions.IsUserAnAdmin((System.Security.Claims.ClaimsPrincipal)User);
            string siteID = await Helpers.CommonFunctions.GetSiteID((System.Security.Claims.ClaimsPrincipal)User);


            if (bIsAdmin)
            {
                Helpers.Audit.Log(siteID, "", "Switch sites", User.Identity.Name + " switched to new site: " + siteID, (System.Security.Claims.ClaimsPrincipal)User);

                ManagementApiClient managementClient = Helpers.CommonFunctions.GetManagementClient();

                Auth0.ManagementApi.Models.UserUpdateRequest user = new Auth0.ManagementApi.Models.UserUpdateRequest();
                
                user.AppMetadata = new Dictionary<string, string>
                    {
                        {"siteID", siteID},
                        {"role","admin" }
                    };



                await managementClient.Users.UpdateAsync(userID, user);




                return Ok();
            }
            else
            {
                return Unauthorized();
            }


        }


        [Authorize]
        [HttpGet, Route("api/user/unblock/{userId}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> unblockUser(string userId)
        {
           
            bool bIsAdmin = await Helpers.CommonFunctions.IsUserAnAdmin((System.Security.Claims.ClaimsPrincipal)User);

            if (bIsAdmin)
            {
                string siteID = await Helpers.CommonFunctions.GetSiteID((System.Security.Claims.ClaimsPrincipal)User);
                Helpers.Audit.Log(siteID, "", "Unblocked User", "Unblocked user: " + userId, (System.Security.Claims.ClaimsPrincipal)User);


                var managementClient = Helpers.CommonFunctions.GetManagementClient();
                //add check for is calling user an admin user
                System.Security.Claims.ClaimsPrincipal oUser = (System.Security.Claims.ClaimsPrincipal)User;


                await managementClient.UserBlocks.UnblockByUserIdAsync(userId);


                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet, Route("api/user/delete/{userId}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> DeleteUser(string userId)
        {
            bool bIsAdmin = await Helpers.CommonFunctions.IsUserAnAdmin((System.Security.Claims.ClaimsPrincipal)User);


            if (bIsAdmin)
            {
                string siteID = await Helpers.CommonFunctions.GetSiteID((System.Security.Claims.ClaimsPrincipal)User);
                Helpers.Audit.Log(siteID, "", "Delete User", "Deleted user: " + userId, (System.Security.Claims.ClaimsPrincipal)User);

                var managementClient = Helpers.CommonFunctions.GetManagementClient();

                await managementClient.Users.DeleteAsync(userId);

                var splits = userId.Split('|');
                var userID = "notfound";

                if (splits.Length> 1)
                {
                    userID = splits[1];
                }

                Helpers.SSOUserHelper.DeleteSSOUserFromDBIfExists(userID);

                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet, Route("api/user/sendVerfiyEmail/{userId}")]
        public async System.Threading.Tasks.Task<IHttpActionResult> sendVerfiyEmail(string userId)
        {
            bool bIsAdmin = await Helpers.CommonFunctions.IsUserAnAdmin((System.Security.Claims.ClaimsPrincipal)User);

            if (bIsAdmin)
            {
                string siteID = await Helpers.CommonFunctions.GetSiteID((System.Security.Claims.ClaimsPrincipal)User);
                Helpers.Audit.Log(siteID, "", "Resent Verfiy", "Resent email for verify email for: " + userId, (System.Security.Claims.ClaimsPrincipal)User);

                var managementClient = Helpers.CommonFunctions.GetManagementClient();
                //add check for is calling user an admin user

                Auth0.ManagementApi.Models.VerifyEmailJobRequest ticket = new Auth0.ManagementApi.Models.VerifyEmailJobRequest();
                ticket.UserId = userId;
                await managementClient.Jobs.SendVerificationEmailAsync(ticket);

                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpPost, Route("api/user/sendPasswordReset")]
        public async System.Threading.Tasks.Task<IHttpActionResult> sendPasswordReset([FromBody] string email)
        {
            bool bIsAdmin = await Helpers.CommonFunctions.IsUserAnAdmin((System.Security.Claims.ClaimsPrincipal)User);

            if (bIsAdmin)
            {
                string siteID = await Helpers.CommonFunctions.GetSiteID((System.Security.Claims.ClaimsPrincipal)User);
                Helpers.Audit.Log(siteID, "", "Password Reset", "Sent password reset request for user: " + email, (System.Security.Claims.ClaimsPrincipal)User);

                var apiClient = GetAuthenticationAPIClient();
                //add check for is calling user an admin user

                Auth0.AuthenticationApi.Models.ChangePasswordRequest request = new Auth0.AuthenticationApi.Models.ChangePasswordRequest();
                request.Email = email;
                request.ClientId = "VPOAb5uOXH9omXmmZuUIZc8OTQ4BFMRW";
                request.Connection = "Username-Password-Authentication";

                await apiClient.ChangePasswordAsync(request);


                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

    }
}