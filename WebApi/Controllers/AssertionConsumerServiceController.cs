﻿using iAuth2._0;
using iAuth2._0.Models;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class AssertionConsumerServiceController : ApiController
    {
        [HttpPost, Route("ACS")]
        public async Task<HttpResponseMessage> HandlePost()
        {

            var context = HttpContext.Current;

            // Checking to verify that a SAML Assertion is included
            if (context.Request.Params["SAMLResponse"] == null)
            {
                return this.getLoginRedirectRespons();
            }


            // pull Base 64 encoded XML saml assertion from Request and decode it
            XmlDocument SAMLXML = new XmlDocument();
            String SAMLResponseString = System.Text.Encoding.UTF8.GetString(
           Convert.FromBase64String(context.Request.Params["SAMLResponse"].ToString()));
            SAMLXML.LoadXml(SAMLResponseString);

            // Validate X509 Certificate Signature
            // if (AssertionConsumerHelper.ValidateX509CertificateSignature(SAMLXML))
            //{
            //      return this.getLoginRedirectRespons();
            // }

            // Finding 
            AssertionType assertion = AssertionConsumerHelper.GetAssertionFromXMLDoc(SAMLXML);

            AssertionData SSOData = null;
            try
            {
                Audit.AddLogData("", "SAMLAttribute:Issuer:" + assertion.Issuer.Value, "", "SSO", "");

            }
            catch (Exception)
            {
                    //do nothing
            }
            
            if (assertion.Issuer.Value== "https://athenanet.athenahealth.com"  
                || assertion.Issuer.Value.Contains("athenanetsso.athenahealth.com")
                || assertion.Issuer.Value.Contains("https://www.okta.com/exkf0235ovDW4o5ew297")
                || assertion.Issuer.Value.Contains("identity.athenahealth.com")
                || assertion.Issuer.Value.Contains("athena")
                || assertion.Issuer.Value.Contains("Okta")
                )
            {
                string name = ((iAuth2._0.Models.NameIDType)assertion.Subject.Items[0]).Value;
                SSOData = new AssertionData(assertion);
                SSOData.SAMLAttributes.Add("name", name);
            }
            else
            {
                return this.getLoginRedirectRespons();
            }

            if (SSOData != null)
            {
                var user = new User();
                var name = "";
                var practiceid = "";
                try
                {
                   name = SSOData.SAMLAttributes["name"];
                }
                catch (Exception)
                {

                    string stringData = string.Join(";", SSOData.SAMLAttributes.Select(x => x.Key + "=" + x.Value).ToArray());
                    Audit.AddLogData("",  "SAMLAttribute:name was not present-data:" + stringData, "","SSO","");
                    throw new Exception("User name was not in SSO");
                }

                try
                {
                   practiceid = SSOData.SAMLAttributes["practiceid"];
                }
                catch (Exception)
                {
                    string stringData = string.Join(";", SSOData.SAMLAttributes.Select(x => x.Key + "=" + x.Value).ToArray());
                    Audit.AddLogData("", "SAMLAttribute:practiceid was not present-data:" + stringData, "", "SSO", "");
                    throw new Exception("Practice ID was not in SSO");
                }
                
               
                 

                user.Username = name + "@AthenaPractice_" + practiceid + ".SSO";
                var patientID = "";
                try
                {
                    patientID = SSOData.SAMLAttributes["legacypatientid"];
                    if (patientID == null)
                    {
                        patientID = "";
                    }

                }
                catch (Exception)
                {
                    
                    Audit.AddLogData("", "SAMLAttribute:legacypatientid was not present" , "", "SSO", user.Username);
                }
                
                user =  await LoginHelper.AuthenticateUser(user);

                

                if (user.access_token != null && user.access_token != "" )
                {
                    var strJSON = JsonConvert.SerializeObject(user);
                    var encodedJSON = Base64Encode(strJSON);
                     var response = Request.CreateResponse(HttpStatusCode.Redirect);
                    //response.Headers.Location = new Uri("https://mdsarchive.com/#/Documents?data=" + encodedJSON);
                    string url = $"https://mdsarchive.com/#access_token={user.access_token}&scope=openid profile full_access&expires_in={user.expiresIn}&token_type={user.token_type}&id_token={user.id_token}&legacypatientid={patientID}";
                    response.Headers.Location = new Uri(url);
                    return response;
                }
                else
                {
                    return this.getLoginRedirectRespons();
                }
            }
            else
            {
                return this.getLoginRedirectRespons();
            }
           


           
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private HttpResponseMessage getLoginRedirectRespons()
        {
            var response = Request.CreateResponse(HttpStatusCode.Redirect);
            response.Headers.Location = new Uri("https://mdsarchive.com/#/login");
            return response;

        }

    }
}
