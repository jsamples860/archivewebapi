﻿using Auth0.AuthenticationApi;
using Auth0.ManagementApi;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class SitesController : ApiController
    {
        [Authorize]
        [HttpGet, Route("api/sites")]
        public async Task<HttpResponseMessage> GetAllSites()
        {
            bool bIsSysAdmin = await Helpers.CommonFunctions.IsSystemAdmin((System.Security.Claims.ClaimsPrincipal)User);
            if (bIsSysAdmin)
            {
                var sites = SiteHelper.GetAllSites();
                return Request.CreateResponse(HttpStatusCode.OK, sites, Configuration.Formatters.JsonFormatter);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }//end GetAllSites

        [Authorize]
        [HttpGet, Route("api/sites/delete/{id}")]
        public async Task<HttpResponseMessage> DeleteSite(string id)
        {

            bool bIsSysAdmin = await Helpers.CommonFunctions.IsSystemAdmin((System.Security.Claims.ClaimsPrincipal)User);
            if (bIsSysAdmin)
            {
                SiteHelper.DeleteSite(id);

                var sites = SiteHelper.GetAllSites();
                return Request.CreateResponse(HttpStatusCode.OK, sites, Configuration.Formatters.JsonFormatter);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [Authorize]
        [HttpPost, Route("api/sites/add")]
        public async Task<HttpResponseMessage> AddSite(Site newSite)
        {

            bool bIsSysAdmin = await Helpers.CommonFunctions.IsSystemAdmin((System.Security.Claims.ClaimsPrincipal)User);
            if (bIsSysAdmin)
            {
                Helpers.SiteHelper.AddSite(newSite);

                var sites = SiteHelper.GetAllSites();
                return Request.CreateResponse(HttpStatusCode.OK, sites, Configuration.Formatters.JsonFormatter);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

        }


        [Authorize]
        [HttpPost, Route("api/sites/edit")]
        public async Task<HttpResponseMessage> EditSite(Site editSite)
        {

            bool bIsSysAdmin = await Helpers.CommonFunctions.IsSystemAdmin((System.Security.Claims.ClaimsPrincipal)User);
            if (bIsSysAdmin)
            {
                Helpers.SiteHelper.EditSite(editSite);

                var sites = SiteHelper.GetAllSites();
                return Request.CreateResponse(HttpStatusCode.OK, sites, Configuration.Formatters.JsonFormatter);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

        }
    }
}