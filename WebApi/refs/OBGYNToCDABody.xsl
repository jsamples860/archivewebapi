<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" omit-xml-declaration = "yes"/>


<xsl:template match="/">
  <xsl:apply-templates select="document"/>
</xsl:template>

<xsl:template match="document">
  <xsl:apply-templates select="episode"/>
</xsl:template>

<xsl:template match="episode">
  <body>
	  <section>
	  	<caption>
	  		Pregnancy Details
	  	</caption>
      <table>
        <tr>
          <td><content>Age/Race:</content></td>
          <td><content><xsl:value-of select="@agerace"/></content></td>
          <td>&#160;&#160;&#160;</td>
          <td>
            <content>Init. Visit:</content>
          </td>
          
          <td>
            <content>
              <xsl:value-of select="@initvisitdate"/>
            </content>
          </td>

        </tr>
        <tr>
          <td><content>Gravida Para:</content></td>
          <td><content><xsl:value-of select="@gravidapara"/></content></td>
          <td>&#160;&#160;&#160;</td>
          <td>
            <content>Init. Weight:</content>
          </td>
          <td>
            <content>
              <xsl:value-of select="@initweight"/>
            </content>
          </td>

        </tr>              
        <tr>
          <td><content>LMP:</content></td>
          <td><content><xsl:value-of select="@lmpdate"/></content></td>
          <td>&#160;&#160;&#160;</td>
          <td>
            <content>Init. BP:</content>
          </td>
          <td>
            <content>
              <xsl:value-of select="@initbpsys"/> / <xsl:value-of select="@initbpdia"/>
            </content>
          </td>

        </tr>
        <tr>
          <td><content>EDD:</content></td>
          <td><content><xsl:value-of select="@edddate"/></content> 
				<xsl:if test="@eddname != ''">
					<content> (<xsl:value-of select="@eddname"/>)</content>
				</xsl:if>
				<xsl:if test="@eddname = ''">
					<content> (LMP)</content>
				</xsl:if>
				<xsl:if test="not(@eddname)">
					<content> (LMP)</content>
				</xsl:if>
          </td>
          <td>&#160;&#160;&#160;</td>
        </tr>
        
                                                                                
      </table>      
	  </section>
    <xsl:apply-templates select="problems"/>	  
    <xsl:apply-templates select="encounters"/>
    <xsl:apply-templates select="education"/>
  </body>
</xsl:template>

<xsl:template match="education">
  <section><!-- Education delivered -->
  	<caption>
  		Education
  	</caption>
  	<list type="unordered">
  	<xsl:for-each select="./*[@enabled='On']">
  	  <item>
  	    <content><xsl:value-of select="@name"/> - <xsl:value-of select="@note"/> - <xsl:value-of select="@date"/> - <xsl:value-of select="@user"/></content>
  	  </item>
  	</xsl:for-each>
  	</list>
  </section>  	
</xsl:template>

<xsl:template match="problems">
  <section><!-- Problems -->
  	<caption>
  		OB Problem List
  	</caption>
  	
  	<list type="unordered">
  	<xsl:for-each select="problem">
  	  <item>
  		<xsl:if test='@enable = "1"'>
  			<content>
  			<xsl:apply-templates />
  			</content>
		</xsl:if> 
  		<xsl:if test='@enable = "0"'>
  			<content>
  			*Not Active: <xsl:apply-templates />
  			</content>
		</xsl:if> 		 	    
  	  </item>
  	</xsl:for-each>
  	</list>
  </section>  	
</xsl:template>

<xsl:template match="encounters">
  <section><!-- encounter records -->
  	<caption>
  		Encounter History
  	</caption>
   <table style="border:1px solid black;padding:1px" cellpadding="4" cellspacing="0">
		<tr>
			<th style="border-right:1px solid black;">
			  Encounter Date Time Provider (Provider Id) [Modified By]
			</th>
			<th style="border-right:1px solid black;">
			  Gest.
			</th>
			<th style="border-right:1px solid black;">
			  Fundal Ht.
			</th>
			<th style="border-right:1px solid black;">
			  Pres.
			</th>
			<th style="border-right:1px solid black;">
			  FHR
			</th>
			<th style="border-right:1px solid black;">
			  Fetal Mov.
			</th>
			<th style="border-right:1px solid black;">
			  Contractions
			</th>
			<th style="border-right:1px solid black;">
			  Cervix Exam
			</th>
			<th style="border-right:1px solid black;">
			  B.P.
			</th>
			<th style="border-right:1px solid black;">
			  Edema
			</th>
			<th style="border-right:1px solid black;">
			  Wt.
			</th>
			<th  style="border-right:1px solid black;">
			  Urine Glu./Protein
			</th>  
			<th  >Pain Scale</th>
		</tr>   
    <xsl:apply-templates select="encounter"/>
  </table>
  </section>    
</xsl:template>

<xsl:template match="encounter">
  <xsl:apply-templates select="version[@versionid=../@lastversionid]"/>
</xsl:template>

<xsl:template match="version">
  <tr>
	<td style="border-top:1px solid black;border-right:1px solid black">
	   <content>
	       <xsl:value-of select="@versiondate"/>&#160;<xsl:value-of select="@versiontime"/>&#160;<xsl:value-of select="@providername"/> (<xsl:value-of select="@providerid"/>)
	       [<xsl:value-of select="@modifiedbyname"/>]
	   </content>
	 </td>
	 <td nowrap="true" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black">
	   <content><xsl:value-of select="@weeksgestation"/></content>
	 </td>
	 <td nowrap="true" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black">
	   <content>&#160;<xsl:value-of select="@fundalheight"/></content>
	 </td>
	 <td nowrap="true" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black">
	   <content>&#160;<xsl:value-of select="@presentationname"/></content>
	 </td>
	 <td nowrap="true" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black">
	   <content>&#160;<xsl:value-of select="@fetalheartrate"/></content>
	 </td>
	 <td nowrap="true" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black">
	   <content>&#160;<xsl:value-of select="@fetalmovement"/></content>
	 </td>
	 <td nowrap="true" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black">
	   <content>&#160;<xsl:value-of select="@pretermlabor"/></content>
	 </td>
	 <td nowrap="true" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black">
	   <content>&#160;<xsl:value-of select="@cervicalexamdil"/></content>
	   /
	   <content><xsl:value-of select="@cervicalexameff"/></content>
	   /
	   <content><xsl:value-of select="@cervicalexamsta"/></content>
	 </td>
	 <td nowrap="true" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black">
	   <content>&#160;<xsl:value-of select="@bloodpressurea"/></content>
	   /
	   <content><xsl:value-of select="@bloodpressureb"/></content>
	 </td>
	 <td nowrap="true" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black">
	   <content>&#160;<xsl:value-of select="@edema"/></content>
	 </td> 
	 <td nowrap="true" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black">
	   <content>&#160;<xsl:value-of select="@weight"/></content>
	 </td>
	 <td nowrap="true" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black">
	   <content>&#160;<xsl:value-of select="@urineglucose"/></content>
	   /
	   <content><xsl:value-of select="@urineprotein"/></content>
	 </td>     
	<td nowrap="true" style="border-top:1px solid black;border-bottom:1px solid black">
	   <content>&#160;<xsl:value-of select="@painscale"/></content>
	 </td>	 
  </tr>
  <tr>
	  <td height="40px"  style="border-right:1px solid black">
		<content>&#160;<xsl:value-of select="@fauxshowborder"/></content>
	  </td> 
	  <td align="right" colspan="2" style="border-right:1px solid black;font-weight:bold">
	    Next Appt:
	  </td>  
	  <td colspan="2" valign="center" style="width:500px; word-wrap:break-word;border-right:1px solid black">
		<content>&#160;<xsl:value-of select="@nextappointment"/></content>
	  </td>	  
	  <td align="right" style="border-right:1px solid black;font-weight:bold">
	    Note:
	  </td>  
	  <td colspan="6" valign="top" style="width:500px; word-wrap:break-word;">
	     <local_markup><datahtml>&#160;<xsl:value-of select="@note"/></datahtml></local_markup>
	  </td>
  </tr>    
</xsl:template>

</xsl:stylesheet>