<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/TR/REC-html40">

<xsl:param name="tabindex" select="1"></xsl:param>
<xsl:strip-space elements="*"/>

<xsl:template match="/">
  <xsl:apply-templates select="document"/>
</xsl:template>

<xsl:template match="document">
  <xsl:apply-templates select="acogdata"/>
</xsl:template>

<xsl:template match="acogdata">
  <xsl:apply-templates select="labs[@version='']"/>
</xsl:template>

<xsl:template  match="labs">
  <table id="ACOGflowsheet" class="tACOGFlowsheet" cellpadding="0" cellspacing="0" style="table-layout:fixed">
    <!-- Display Initial Labs -->
      <xsl:call-template name="typeRow">
        <xsl:with-param name="labtype" select="'initial'"/>
        <xsl:with-param name="labtitle" select="'Initial Labs '"/>
      </xsl:call-template>
       <xsl:apply-templates select="lab[@type='initial']">
        <xsl:with-param name="labCounter" select="100"/>
      </xsl:apply-templates>

    <!-- Display Optional Labs -->
      <xsl:call-template name="typeRow">
        <xsl:with-param name="labtype" select="'optional'"/>
        <xsl:with-param name="labtitle" select="'Optional Labs '"/>
      </xsl:call-template>
      <xsl:apply-templates select="lab[@type='optional']">
        <xsl:with-param name="labCounter" select="200"/>
      </xsl:apply-templates>

    <!-- Display 8-18 week Labs -->
      <xsl:call-template name="typeRow">
        <xsl:with-param name="labtype" select="'8-18'"/>
        <xsl:with-param name="labtitle" select="'8-18 Week Labs '"/>
      </xsl:call-template>
      <xsl:apply-templates select="lab[@type='8-18']">
        <xsl:with-param name="labCounter" select="300"/>
      </xsl:apply-templates>

    <!-- Display 24-28 week Labs -->
      <xsl:call-template name="typeRow">
        <xsl:with-param name="labtype" select="'24-28'"/>
        <xsl:with-param name="labtitle" select="'24-28 Week Labs '"/>
      </xsl:call-template>
      <xsl:apply-templates select="lab[@type='24-28']">
        <xsl:with-param name="labCounter" select="400"/>
      </xsl:apply-templates>

    <!-- Display 32-36 week Labs -->
      <xsl:call-template name="typeRow">
        <xsl:with-param name="labtype" select="'32-36'"/>
        <xsl:with-param name="labtitle" select="'32-36 Week Labs '"/>
      </xsl:call-template>
      <xsl:apply-templates select="lab[@type='32-36']">
        <xsl:with-param name="labCounter" select="500"/>
      </xsl:apply-templates>
    
  </table>
</xsl:template>

<!--  Display the type or category header row  -->
<xsl:template  name="typeRow">
  <xsl:param name ="labtype"/>
  <xsl:param name ="labtitle"/>
  <tr class="" onclick="toggleNugget" >
      <xsl:attribute name="id"><xsl:value-of  select="$labtitle"/></xsl:attribute>
    <td class="labtypeheader" style="padding-left:5px;padding-top:3px" width="275px"><xsl:value-of select="$labtitle"/>
      <span onclick="addLab me">
		<xsl:attribute name="id"><xsl:value-of  select="$labtype"/></xsl:attribute>
      <img src="/primepractice/clinical/images/arrowrite.gif" style="margin-left:80px;margin-right:10px"   title="Add a lab">
      </img>
      <u>Add</u>
      </span>
    </td>
    <td class="labtypeheader" style="padding-left:5px" width="105px">Date</td>
    <td class="labtypeheader" style="padding-left:5px" width="190px">Result</td>
    <td class="labtypeheader" style="padding-left:5px" width="80px">By User</td>    
    <td class="labtypeheader" style="padding-left:5px" width="140px">Reviewed</td>
    <td class="labtypeheader" style="padding-left:3px" width="39px">Note</td>    
    </tr>
</xsl:template>

<!-- Process Lab level display -->
<xsl:template  match="lab">
  <xsl:param name="labCounter"/>
  <xsl:variable name="bRowEnabled" select="string-length(review/@reviewer_id) = 0"/>
    <tr class="labRow_disabled" >
      <xsl:attribute name="id"><xsl:value-of  select="@type"/></xsl:attribute>
      <xsl:attribute name="childindex"><xsl:value-of  select="position()"/></xsl:attribute>
      <xsl:if test="$bRowEnabled">
        <xsl:attribute name="class">labRow</xsl:attribute>
      </xsl:if>  
      <xsl:attribute name="style">display:<xsl:value-of  select="@display"/></xsl:attribute>
    <td class="rowlabel" style="padding-left:3px">
         <xsl:attribute name="labName"><xsl:value-of select="@name" /></xsl:attribute>
       <xsl:value-of  select="@description"/>
     </td>

     <xsl:apply-templates select="result">
       <xsl:with-param name="labCounter" select="$labCounter+(position()*10)"/>
       <xsl:with-param name="bRowEnabled" select="$bRowEnabled"/>
     </xsl:apply-templates>
     
     
     <xsl:apply-templates select="review">
       <xsl:with-param name="labCounter" select="$labCounter+(position()*10)"/>
       <xsl:with-param name="bRowEnabled" select="$bRowEnabled"/>
    </xsl:apply-templates>
    
     <xsl:apply-templates select="note">
       <xsl:with-param name="labCounter" select="$labCounter+(position()*10)"/>
       <xsl:with-param name="bRowEnabled" select="$bRowEnabled"/>
    </xsl:apply-templates>    
  </tr>
</xsl:template>

<!-- This template is not currently used-->
<xsl:template  match="order">
  <td class="dataCell">
    <div id="order" childPath="order" class="divData" >
        <xsl:attribute name="labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute> 
      <xsl:value-of select="child::result" />
    </div>
  </td>
</xsl:template>

<xsl:template  match="result">
  <xsl:param name="labCounter"/>
  <xsl:param name="bRowEnabled"/><!-- When true, this will put div in tab order, default is -1 -->
  <xsl:variable name="sRender_as"><!--This variable holds value that controls the popup dialog type -->
    <xsl:value-of select="parent::lab/@render_as" />
  </xsl:variable>
  
  <td class="dataCell" onclick="doClickNavigation"><!-- This cell holds the Date the lab results were returned -->
      <span class="divDlgImg2" language="vbscript" onfocus="doCellNavigation('focus')" onblur="doCellNavigation('blur')" render_as="date" tabindex="-1" style="">
          <xsl:attribute name="id">dlg.<xsl:value-of select="parent::lab/@name" />.result.dt_received</xsl:attribute>
          <xsl:attribute name="labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute>
          <xsl:if test="$bRowEnabled">
            <xsl:attribute name="tabindex"><xsl:value-of select="$labCounter+1" /></xsl:attribute>
          </xsl:if>
          <xsl:attribute name="tabidx"><xsl:value-of select="$labCounter+1" /></xsl:attribute>
			
			
			<img src="/PrimePractice/Clinical/FlowSheets/OBGYN/images/smallcalender.bmp" style="cursor:hand"  title="Show Date dialog" tabindex="-1" >
			  <xsl:attribute name="labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute>
			  <!--<xsl:attribute name="onmouseover">vbscript:me.src="/primepractice/clinical/images/icons/dlg_date_roll.gif"</xsl:attribute>
			  <xsl:attribute name="onmouseout">vbscript:me.src="/primepractice/clinical/images/icons/dlg_date.gif"</xsl:attribute>-->            
			</img>
			
      </span>  
      <span id="received_dt" childPath="result.received_dt" class="divData" onkeyup="updateLabResult" >
          <xsl:attribute name="labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute>
          <xsl:attribute name="resultValue"><xsl:value-of select="@received_dt"/></xsl:attribute> 
        <xsl:value-of select="@received_dt"/>
      </span>
     </td>
  <td class="dataCell" onclick="doClickNavigation"><!-- This cell holds the actual lab results -->
    <div class="divDlgImg" language="vbscript" onfocus="doCellNavigation('focus')" onblur="doCellNavigation('blur')" tabindex="-1" >
        <xsl:attribute name="id">dlg.<xsl:value-of select="parent::lab/@name" />.result.text</xsl:attribute>
        <xsl:attribute name="render_as"><xsl:value-of  select="$sRender_as"/></xsl:attribute>
        <xsl:attribute name="labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute>
        <xsl:attribute name="childPath">result</xsl:attribute>        
        <xsl:if test="$bRowEnabled">
          <xsl:attribute name = "tabindex"><xsl:value-of select="$labCounter+2" /></xsl:attribute>
        </xsl:if>
        <xsl:attribute name = "tabidx"><xsl:value-of select="$labCounter+2" /></xsl:attribute>
      <img  border="0" style="cursor:hand"  tabindex="-1">
          <xsl:attribute name="src">/primepractice/clinical/images/icons/dlg_<xsl:value-of  select="$sRender_as"/>.gif</xsl:attribute>
          <xsl:attribute name="title">Show <xsl:value-of  select="$sRender_as"/> dialog</xsl:attribute>
          <xsl:attribute name="labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute>
        <!--<xsl:if test="$bRowEnabled">
          <xsl:attribute name="onmouseover">vbscript:me.src="/primepractice/clinical/images/icons/dlg_<xsl:value-of  select="$sRender_as"/>_roll.gif"</xsl:attribute>
          <xsl:attribute name="onmouseout">vbscript:me.src="/primepractice/clinical/images/icons/dlg_<xsl:value-of  select="$sRender_as"/>.gif"</xsl:attribute>           
        </xsl:if>-->
      </img>
    </div>        
    <!--We ALWAYS output the result string Div --> 
    <div id="result" style="padding-left:20px" childPath="result" class="divData" onkeyup="updateLabResult">
        <xsl:attribute name="labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute>
        <xsl:attribute name="resultValue"><xsl:value-of select="."/></xsl:attribute> 
        <xsl:call-template name="linebreaks">
            <xsl:with-param name="string" select="."/>
        </xsl:call-template>      
    </div>
  </td>
  <td class="dataCell" >  
    <div id="inputuser" class="divData" tabindex="-1" style="padding-left:3px;padding-right:3px">
        <xsl:attribute name="title"><xsl:value-of select="@user_name"/></xsl:attribute>
      <xsl:value-of select="@user_name"/>&#160;
    </div> 
  </td>  
  
</xsl:template>


<xsl:template  match="review">
  <xsl:param name="labCounter"/>
  <xsl:param name="bRowEnabled"/>
    <xsl:attribute name="labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute>
  <td class="dataCell"  align="right">
    <xsl:choose><!-- Which button to display: initial or revoke -->
      <xsl:when test="$bRowEnabled"><!-- This lab has been initialed -->
        <div class="divDlgImg" childPath="review.reviewer_id" language="vbscript" onclick="setReview" tabindex="-1">
            <xsl:attribute name="id"><xsl:value-of select="parent::lab/@name" />-review</xsl:attribute>
            <xsl:if test="(string-length(parent::lab/result/@received_dt ) = 0)  or (string-length(parent::lab/result ) = 0)">
              <xsl:attribute name="style">visibility:hidden</xsl:attribute>
            </xsl:if>            
            <xsl:attribute name = "labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute>             
          <img  border="0" style="cursor:hand" src="/primepractice/clinical/images/icons/btn_initial.gif" tabindex="-1">
           <xsl:attribute name="title">Click when you have reviewed this lab result</xsl:attribute>
          </img>
        </div>      
      </xsl:when>
      
      <xsl:otherwise>
        <div class="divDlgImg" childPath="review.reviewer_id" language="vbscript" onclick="setReview" tabindex="-1">
            <xsl:attribute name="id"><xsl:value-of select="parent::lab/@name" />-review</xsl:attribute>
            <xsl:attribute name = "labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute> 
          <img border="0" style="cursor:hand" src="/primepractice/clinical/images/icons/btn_initialrevoke.gif" tabindex="-1">
           <xsl:attribute name="title">Click to revoke review and enable editing</xsl:attribute>
          </img>
        </div>      
      </xsl:otherwise>
    </xsl:choose>    
    <div id="review" class="divData" tabindex="-1" style="margin-right:4px">
      <xsl:value-of select="@reviewer_name"/>
    </div>    

  </td>
</xsl:template>


<xsl:template  match="note">
  <xsl:param name="labCounter"/>
  <xsl:param name="bRowEnabled"/>
  <xsl:variable name="sRender_as">Text-Memo</xsl:variable>
  <td class="dataCell" ><!-- This cell holds the note image -->
    <div class="divDlgImgNote" style="padding:3px" onclick="showNote(me)"  childPath="result" language="vbscript" render_as="memo" tabindex="-1">
      <xsl:attribute name="id">dlg.<xsl:value-of select="parent::lab/@name" />.note</xsl:attribute>
      <xsl:attribute name="labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute>
      <img style="cursor:hand;"  title="Show Note dialog" tabindex="-1" >
        <xsl:attribute name="labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute>
        <xsl:choose>        
          <xsl:when test="string-length(.)>0">
            <xsl:attribute name="src">/PrimePractice/Clinical/FlowSheets/OBGYN/images/notesmall.gif</xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="src">/PrimePractice/Clinical/FlowSheets/OBGYN/images/notesmalldisabled.gif</xsl:attribute>
          </xsl:otherwise>               
        </xsl:choose>              
      </img>
    </div>
    <div id="note"  childPath="note" class="divData" style="display:none"  onkeyup="updateNote">
        <xsl:attribute name="labName"><xsl:value-of select="parent::lab/@name" /></xsl:attribute>
        <xsl:attribute name="resultValue"><xsl:value-of select="."/></xsl:attribute> 
        <xsl:copy-of select="."/>
    </div> 
  </td>

</xsl:template>

<xsl:template name="linebreaks">
    <xsl:param name="string"/>
    <xsl:choose>
        <xsl:when test="contains($string, ';')">
            <xsl:value-of select="substring-before($string, ';')"/>
            <br/>
            <xsl:call-template name="linebreaks">
                <xsl:with-param name="string" select="substring-after($string, ';')"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$string"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>